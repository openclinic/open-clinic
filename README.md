# Open Clinic
Open Clinic is a backend application written in Java using Spring framework.
It is a modularized monolith written with future improvements in mind. It is a data processing 
middle tier web REST API used in Open Clinic patient's portal to provide and manage all necessary data 
like doctors, patients, prescriptions, visits etc. It is integrated with Angular FE application(Still
can be used by any client capable of communicating with a REST API) and MYSQL database used as a reliable persistent data
storage.
## Getting Started
To start open clinic on your local machine you need to clone
the OP FE and BE repositories from OP bitbucket project. Backend with database can be run with docker-compose file
located in the root of a project's folder. Frontend initialization is described in the FE repository.
Before running docker compose you have to compile all elements and build docker images for them.
```
open-clinic/ mvn clean package
```
```
open-clinic/ docker build -t com.bwas.openclinic/open-clinic:latest .
```
```
open-clinic/ docker-compose up
```
### Prerequisites
* Docker dekstop 
* Maven
* Java 8

 how to get - [Docker](https://www.docker.com/products/docker-desktop)
 
 how to get - [Maven](https://maven.apache.org/) 
 
 how to get - [JAVA](https://www.oracle.com/java/technologies/javase-jdk8-downloads.html) 
## Running the tests
You can run tests using Maven command:
```
open-clinic/ mvn test
```
## Deployment
The project is using CD pipelines. After merging to development or master branch the application image will deployed to AWS Elastic Beanstalk hosting automatically.
## Versioning
We use [BitBucket](https://bitbucket.org)  Git versioning platform. Get newest version of this service from our [OC project repository](https://bitbucket.org/openclinic/open-clinic/src/development/).
## Quality Control
For controlling quality of our services we are using [SonarCloud](https://sonarcloud.io/dashboard?id=openclinic_open-clinic) platform. It checks our code for bugs and potential security issues.
It also checks our code coverage. Quality check is performed by sonar cloud in bitbucket pipelines after each merge to development branch.
## Acknowledgments
* **Bartosz Waś**
