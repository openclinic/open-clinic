FROM openjdk:8-alpine

COPY target/open-clinic.jar /tmp/app.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/tmp/app.jar"]
