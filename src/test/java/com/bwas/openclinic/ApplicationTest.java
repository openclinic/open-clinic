package com.bwas.openclinic;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "visit.length=30")
public class ApplicationTest {

	@Test
	public void contextLoads() {
	}

}
