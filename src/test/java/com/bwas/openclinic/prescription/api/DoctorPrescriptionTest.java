package com.bwas.openclinic.prescription.api;

import com.bwas.openclinic.AbstractTest;
import com.bwas.openclinic.shared.DataPage;
import com.bwas.openclinic.user.doctor.domain.Specialization;
import com.bwas.openclinic.user.domain.entity.Role;
import com.bwas.openclinic.user.patient.domain.Sex;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "visit.length=30")
public class DoctorPrescriptionTest extends AbstractTest {

    private static final UUID PATIENT_UUID = UUID.randomUUID();
    private static final UUID DOCTOR_UUID = UUID.randomUUID();

    @Autowired
    private DoctorPrescriptionController doctorPrescriptionController;

    @Autowired
    private DataSource dataSource;

    @Before
    public void setupDb() {
        clearDb();

        addDoctor(
                1,
                DOCTOR_UUID,
                "doctor@clinic.com",
                "Bartosz",
                "Was",
                "111111111",
                Role.DOCTOR,
                "password",
                true,
                "description",
                Specialization.SURGERY
        );

        addPatient(
                2,
                PATIENT_UUID,
                "patient@clinic.com",
                "Marika",
                "Romocka",
                "111111119",
                Role.PATIENT,
                "password1",
                true,
                Sex.FEMALE,
                "123432343234",
                "2020-01-03",
                1l
        );

    }

    @Test
    @WithMockUser(roles = "DOCTOR")
    public void findAllForDoctor() {
        //given
        UUID prescriptionUUID = UUID.randomUUID();
        addPrescription(
                5,
                prescriptionUUID,
                "ADG234",
                "2020-01-03",
                "2020-04-03",
                "Insulin",
                "2 times a day int the morning and evening",
                2l,
                1l);

        //when
        DataPage<PrescriptionDTO> prescriptions = doctorPrescriptionController.findAll(0,4,"medicineName", Sort.Direction.ASC, DOCTOR_UUID);

        //then
        Assertions.assertEquals(prescriptions.getContent().get(0).getUuid(), prescriptionUUID);
    }

    @Test
    @WithMockUser(roles = "DOCTOR")
    public void create() {
        //given
        NewPrescriptionDTO newPrescription = NewPrescriptionDTO.builder()
                .medicineName("Insulin")
                .description("2 times a day int the morning and evening")
                .doctorUUID(DOCTOR_UUID)
                .patientUUID(PATIENT_UUID)
                .build();

        //when
        PrescriptionDTO createdPrescription = doctorPrescriptionController.create(newPrescription);

        Long prescriptionsCount = new JdbcTemplate(dataSource).queryForObject("select count(*) from prescription where medicine_name ='" + newPrescription.getMedicineName() + "'", Long.class);

        //then
        Assert.assertEquals(1l, (long)prescriptionsCount);
        Assert.assertEquals(newPrescription.getMedicineName(), createdPrescription.getMedicineName());
        Assert.assertEquals(newPrescription.getDescription(), createdPrescription.getDescription());
    }
}
