package com.bwas.openclinic.user.patient.api;

import com.bwas.openclinic.AbstractTest;
import com.bwas.openclinic.exceptions.NotFoundException;
import com.bwas.openclinic.shared.DataPage;
import com.bwas.openclinic.user.doctor.domain.Specialization;
import com.bwas.openclinic.user.domain.entity.Role;
import com.bwas.openclinic.user.patient.domain.Sex;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "visit.length=30")
public class PatientTest extends AbstractTest {
    private static final UUID PATIENT_UUID = UUID.randomUUID();
    private static final UUID DOCTOR_UUID = UUID.randomUUID();

    @Autowired
    private PatientController patientController;

    @Before
    public void setupDb() {
        clearDb();

        addDoctor(
                1,
                DOCTOR_UUID,
                "doctor@clinic.com",
                "Bartosz",
                "Was",
                "111111111",
                Role.DOCTOR,
                "password",
                true,
                "description",
                Specialization.SURGERY
        );

        addPatient(
                2,
                PATIENT_UUID,
                "patient@clinic.com",
                "Marika",
                "Romocka",
                "111111119",
                Role.PATIENT,
                "password1",
                true,
                Sex.FEMALE,
                "123432343234",
                "2020-01-03",
                1l
        );
    }

    @Test
    @WithMockUser(roles = "PATIENT")
    public void findByUUID() {
        //when
        PatientDTO patient = patientController.find(PATIENT_UUID);

        //then
        Assertions.assertEquals(PATIENT_UUID, patient.getUuid());
    }

    @Test(expected = NotFoundException.class)
    @WithMockUser(roles = "PATIENT")
    public void findByWronUUID() {
        //when
        PatientDTO patient = patientController.find(UUID.randomUUID());
    }


    @Test
    @WithMockUser(roles = "DOCTOR")
    public void findAllForDoctor() {
        //when
        DataPage<PatientDTO> patients = patientController.findAll(0,4,"firstName", Sort.Direction.ASC, DOCTOR_UUID);

        //then
        Assertions.assertEquals(PATIENT_UUID, patients.getContent().get(0).getUuid());
    }
}
