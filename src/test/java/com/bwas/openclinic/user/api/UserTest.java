package com.bwas.openclinic.user.api;

import com.bwas.openclinic.AbstractTest;
import com.bwas.openclinic.user.domain.entity.Role;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "visit.length=30")
public class UserTest extends AbstractTest {

    private static final UUID USER_UUID = UUID.randomUUID();

    @Autowired
    private UserController userController;

    @Before
    public void setupDb() {
        clearDb();

        addUser(
                10,
                USER_UUID,
                "user@clinic.com",
                "John",
                "Doe",
                "121212122",
                Role.DOCTOR,
                "password3",
                true
        );
    }

    @Test
    public void updateImage() throws IOException {
        //given
        MultipartFile photo = new MultipartFile() {
            @Override
            public String getName() {
                return null;
            }

            @Override
            public String getOriginalFilename() {
                return null;
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return 0;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return new byte[0];
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public void transferTo(File file) throws IOException, IllegalStateException {

            }
        };

        //when
        userController.uploadImage(USER_UUID, photo);
    }

    @Test
    public void update() {
        //given
        UserUpdateDTO userUpdate = UserUpdateDTO.builder()
                .email("updatedUser@clinic.com")
                .password("newPassword")
                .phoneNumber("211111111")
                .build();

        //when
        UserDTO updatedUser = userController.update(USER_UUID, userUpdate);

        //then
        Assert.assertEquals(USER_UUID, updatedUser.getUuid());
        Assert.assertEquals(userUpdate.getEmail(), updatedUser.getEmail());
        Assert.assertEquals(userUpdate.getPhoneNumber(), updatedUser.getPhoneNumber());
    }

    @Test
    public void updateWithEmptyPassword() {
        //given
        UserUpdateDTO userUpdate = UserUpdateDTO.builder()
                .email("")
                .password("")
                .phoneNumber("211111111")
                .build();

        //when
        UserDTO updatedUser = userController.update(USER_UUID, userUpdate);

        //then
        Assert.assertEquals(USER_UUID, updatedUser.getUuid());
        Assert.assertEquals(userUpdate.getEmail(), updatedUser.getEmail());
        Assert.assertEquals(userUpdate.getPhoneNumber(), updatedUser.getPhoneNumber());
    }
}
