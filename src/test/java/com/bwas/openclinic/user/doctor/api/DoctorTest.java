package com.bwas.openclinic.user.doctor.api;

import com.bwas.openclinic.AbstractTest;
import com.bwas.openclinic.user.doctor.domain.Specialization;
import com.bwas.openclinic.user.domain.entity.Role;
import com.bwas.openclinic.user.patient.domain.Sex;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "visit.length=30")
public class DoctorTest extends AbstractTest {
    private static final UUID PATIENT_UUID = UUID.randomUUID();
    private static final UUID DOCTOR_UUID = UUID.randomUUID();
    private static final UUID CLINIC_UUID = UUID.randomUUID();
    private static final UUID SCHEDULE_UUID = UUID.randomUUID();

    @Autowired
    private DoctorController doctorController;

    @Before
    public void setupDb() {
        clearDb();

        addClinic(
                3,
                CLINIC_UUID,
                "Saints Cross clinic",
                "Cross street",
                "London",
                "E1 6AN",
                "111111111");


        addDoctor(
                1,
                DOCTOR_UUID,
                "doctor@clinic.com",
                "Bartosz",
                "Was",
                "111111111",
                Role.DOCTOR,
                "password",
                true,
                "description",
                Specialization.SURGERY
        );

        addPatient(
                2,
                PATIENT_UUID,
                "patient@clinic.com",
                "Marika",
                "Romocka",
                "111111119",
                Role.PATIENT,
                "password1",
                true,
                Sex.FEMALE,
                "123432343234",
                "2020-01-03",
                1l
        );

        addSchedule(4,
                SCHEDULE_UUID,
                "2020-01-03",
                "2020-02-03",
                true,
                1l,
                3l
        );

    }

    @Test
    @WithMockUser(roles = "PATIENT")
    public void findForPatient() {
        //when
        DoctorDTO doctor = doctorController.find(PATIENT_UUID);

        //then
        Assertions.assertEquals(DOCTOR_UUID, doctor.getUuid());
    }

    @Test
    @WithMockUser(roles = "PATIENT")
    public void findByUUID() {
        //when
        DoctorDTO doctor = doctorController.findByUUID(DOCTOR_UUID);

        //then
        Assertions.assertEquals(DOCTOR_UUID, doctor.getUuid());
    }

    @Test
    @WithMockUser(roles = "PATIENT")
    public void findAllSpecializations() {
        //when
        List<String> specializations = doctorController.findAllSpecializations();

        //then
        Assertions.assertFalse(specializations.isEmpty());
    }

    @Test
    @WithMockUser(roles = "PATIENT")
    public void findAllBySpecializations() {
        //when
        List<DoctorDTO> doctors = doctorController.findAll(Specialization.SURGERY);

        //then
        Assertions.assertEquals(DOCTOR_UUID, doctors.get(0).getUuid());
    }

    @Test
    @WithMockUser(roles = "PATIENT")
    public void findAll() {
        //when
        List<DoctorDTO> doctors = doctorController.findAll();

        //then
        Assertions.assertEquals(DOCTOR_UUID, doctors.get(0).getUuid());
    }

    @Test
    @WithMockUser(roles = "PATIENT")
    public void findAllAvailableDoctorsForBooking() {
        //when
        List<DoctorDTO> doctors = doctorController.findAll(LocalDateTime.of(2020, 5, 22, 10, 0, 0), Specialization.SURGERY, CLINIC_UUID);

        //then
        Assertions.assertEquals(DOCTOR_UUID, doctors.get(0).getUuid());
    }
}
