package com.bwas.openclinic.clinic.api;

import com.bwas.openclinic.AbstractTest;
import com.bwas.openclinic.exceptions.NotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "visit.length=30")
public class ClinicTest extends AbstractTest {

    @Autowired
    private ClinicController clinicController;

    @Before
    public void setupDb() {
        clearDb();
    }

    @Test
    @WithMockUser(roles = "PATIENT")
    public void findByUUID() {
        //given
        UUID clinicUUID = UUID.randomUUID();
        addClinic(1, clinicUUID, "Saints Cross clinic", "Cross street", "London", "E1 6AN", "111111111");


        //when
        ClinicDTO actualClinic = clinicController.find(clinicUUID);

        //then
        Assertions.assertEquals(actualClinic.getUuid(), clinicUUID);
    }

    @Test(expected = NotFoundException.class)
    @WithMockUser(roles = "PATIENT")
    public void findByWrongUUID() {
        //given
        UUID clinicUUID = UUID.randomUUID();

        //when
        clinicController.find(clinicUUID);
    }

    @Test
    @WithMockUser(roles = "PATIENT")
    public void findAll() {
        //given
        UUID clinicUUID = UUID.randomUUID();
        addClinic(1, clinicUUID, "West London Clinic", "West street", "London", "E1 6AN", "222222222");

        UUID clinic2UUID = UUID.randomUUID();
        addClinic(2, clinic2UUID, "Saints Cross clinic", "Cross street", "London", "E1 6AN", "111111111");

        //when
        List<ClinicDTO> actualClinics = clinicController.findAll();

        //then
        Assertions.assertEquals(actualClinics.get(0).getUuid(), clinicUUID);
        Assertions.assertEquals(actualClinics.get(1).getUuid(), clinic2UUID);
    }
}
