package com.bwas.openclinic.visit.api;

import com.bwas.openclinic.AbstractTest;
import com.bwas.openclinic.shared.DataPage;
import com.bwas.openclinic.user.doctor.domain.Specialization;
import com.bwas.openclinic.user.domain.entity.Role;
import com.bwas.openclinic.user.patient.domain.Sex;
import com.bwas.openclinic.visit.VisitStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "visit.length=30")
public class VisitTest extends AbstractTest {

    private static final UUID PATIENT_UUID = UUID.randomUUID();
    private static final UUID DOCTOR_UUID = UUID.randomUUID();
    private static final UUID CLINIC_UUID = UUID.randomUUID();
    private static final UUID VISIT_UUID = UUID.randomUUID();
    private static final UUID SCHEDULE_UUID = UUID.randomUUID();

    @Autowired
    private VisitController visitController;

    @Autowired
    private DataSource dataSource;

    @Before
    public void setupDb() {
        clearDb();

        addDoctor(
                1,
                DOCTOR_UUID,
                "doctor@clinic.com",
                "Bartosz",
                "Was",
                "111111111",
                Role.DOCTOR,
                "password",
                true,
                "description",
                Specialization.SURGERY
        );

        addPatient(
                2,
                PATIENT_UUID,
                "patient@clinic.com",
                "Marika",
                "Romocka",
                "111111119",
                Role.PATIENT,
                "password1",
                true,
                Sex.FEMALE,
                "123432343234",
                "2020-01-03",
                1l
        );

        addClinic(
                3,
                CLINIC_UUID,
                "Saints cross clinic",
                "Cross street",
                "London",
                "23124",
                "333333333"
        );

        addVisit(
                4,
                VISIT_UUID,
                "2020-05-22 10:00:00",
                "2020-05-22 10:20:00",
                VisitStatus.BOOKED,
                BigDecimal.valueOf(2000),
                "Surgery",
                3l,
                2l,
                1l
        );

        addSchedule(4,
                SCHEDULE_UUID,
                "2020-01-03",
                "2020-02-03",
                true,
                1l,
                3l
        );
    }

    @Test
    @WithMockUser(roles = "PATIENT")
    public void findAllForPatient() {
        //when
        DataPage<VisitDTO> visits = visitController.findAll(0,4,"startDate", Sort.Direction.ASC, PATIENT_UUID);

        //then
        Assertions.assertEquals(visits.getContent().get(0).getUuid(), VISIT_UUID);
    }

    @Test
    @WithMockUser(roles = "PATIENT")
    public void findAllForPatientAndStatus() {
        //when
        DataPage<VisitDTO> visits = visitController.findAllWithStatus(0,4,"startDate", Sort.Direction.ASC, PATIENT_UUID, VisitStatus.BOOKED);

        //then
        Assertions.assertEquals(visits.getContent().get(0).getUuid(), VISIT_UUID);
    }

    @Test
    @WithMockUser(roles = "PATIENT")
    public void findByUUID() {
        //when
        VisitDTO visit = visitController.find(VISIT_UUID);

        //then
        Assertions.assertEquals(VISIT_UUID, visit.getUuid());
    }

    @Test
    @WithMockUser(roles = "PATIENT")
    public void update() {
        //when
        VisitDTO visit = visitController.update(VISIT_UUID, VisitStatus.FINISHED);

        //then
        Assertions.assertEquals(VISIT_UUID, visit.getUuid());
        Assertions.assertEquals(VisitStatus.FINISHED, visit.getStatus());
    }

    @Test
    @WithMockUser(roles = "PATIENT")
    public void create() {
        //given
       NewVisitDTO newVisitDTO = NewVisitDTO.builder()
               .clinicUUID(CLINIC_UUID)
               .doctorUUID(DOCTOR_UUID)
               .patientUUID(PATIENT_UUID)
               .startDate(LocalDateTime.of(2020, 1, 5, 12, 0, 0))
               .build();

        //when
        VisitDTO newVisit = visitController.create(newVisitDTO);

        Long visitsCount = new JdbcTemplate(dataSource).queryForObject("select count(*) from visit where start_date ='" + newVisitDTO.getStartDate() + "'", Long.class);

        //then
        Assert.assertEquals((long)visitsCount, 1l);
        Assert.assertEquals(newVisitDTO.getClinicUUID(), newVisit.getClinic().getUuid());
        Assert.assertEquals(newVisitDTO.getStartDate(), newVisit.getStartDate());
    }

}
