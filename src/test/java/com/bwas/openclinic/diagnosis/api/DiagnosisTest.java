package com.bwas.openclinic.diagnosis.api;

import com.bwas.openclinic.AbstractTest;
import com.bwas.openclinic.shared.DataPage;
import com.bwas.openclinic.user.doctor.domain.Specialization;
import com.bwas.openclinic.user.domain.entity.Role;
import com.bwas.openclinic.user.patient.domain.Sex;
import com.bwas.openclinic.visit.VisitStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "visit.length=30")
public class DiagnosisTest extends AbstractTest {

    private static final UUID PATIENT_UUID = UUID.randomUUID();
    private static final UUID DOCTOR_UUID = UUID.randomUUID();
    private static final UUID CLINIC_UUID = UUID.randomUUID();
    private static final UUID VISIT_UUID = UUID.randomUUID();

    @Autowired
    private DiagnosisController diagnosisController;

    @Autowired
    private DataSource dataSource;

    @Before
    public void setupDb() {
        clearDb();

        addDoctor(
                1,
                DOCTOR_UUID,
                "doctor@clinic.com",
                "Bartosz",
                "Was",
                "111111111",
                Role.DOCTOR,
                "password",
                true,
                "description",
                Specialization.SURGERY
        );

        addPatient(
                2,
                PATIENT_UUID,
                "patient@clinic.com",
                "Marika",
                "Romocka",
                "111111119",
                Role.PATIENT,
                "password1",
                true,
                Sex.FEMALE,
                "123432343234",
                "2020-01-03",
                1l
        );

        addClinic(
                3,
                CLINIC_UUID,
                "Saints cross clinic",
                "Cross street",
                "London",
                "23124",
                "333333333"
        );

        addVisit(
                4,
                VISIT_UUID,
                "2020-05-22 10:00:00",
                "2020-05-22 10:20:00",
                VisitStatus.BOOKED,
                BigDecimal.valueOf(2000),
                "Surgery",
                3l,
                2l,
                1l
        );
    }

    @Test
    @WithMockUser(roles = "PATIENT")
    public void findAllForPatient() {
        //given
        UUID diagnosisUUID = UUID.randomUUID();
        addDiagnosis(5, diagnosisUUID, "Broken Arm", "Open wound", "2020-01-03", 1l,4l,2l);

        //when
        DataPage<DiagnosisDTO> diagnosis = diagnosisController.findAll(0,4,"name", Sort.Direction.ASC, PATIENT_UUID);

        //then
        Assertions.assertEquals(diagnosis.getContent().get(0).getUuid(), diagnosisUUID);
        Assertions.assertEquals(PATIENT_UUID, diagnosis.getContent().get(0).getPatientUUID());
        Assertions.assertEquals( DOCTOR_UUID, diagnosis.getContent().get(0).getDoctorUUID());
    }

    @Test
    @WithMockUser(roles = "DOCTOR")
    public void create() {
        //given
        NewDiagnosisDTO newDiagnosis = NewDiagnosisDTO.builder()
                .name("Broken Leg")
                .description("Broken in 4 places")
                .doctorUUID(DOCTOR_UUID)
                .patientUUID(PATIENT_UUID)
                .visitUUID(VISIT_UUID)
                .build();

        //when
        DiagnosisDTO createdDiagnosis = diagnosisController.create(newDiagnosis);

        Long diagnosisCount = new JdbcTemplate(dataSource).queryForObject("select count(*) from diagnosis where name ='" + newDiagnosis.getName() + "'", Long.class);

        //then
        Assert.assertEquals(1l, (long)diagnosisCount);
        Assert.assertEquals(newDiagnosis.getDoctorUUID(), createdDiagnosis.getDoctorUUID());
        Assert.assertEquals(newDiagnosis.getPatientUUID(), createdDiagnosis.getPatientUUID());
        Assert.assertEquals(newDiagnosis.getVisitUUID(), createdDiagnosis.getVisitUUID());
    }
}
