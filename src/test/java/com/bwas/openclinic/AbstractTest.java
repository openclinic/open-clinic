package com.bwas.openclinic;

import com.bwas.openclinic.user.doctor.domain.Specialization;
import com.bwas.openclinic.user.domain.entity.Role;
import com.bwas.openclinic.user.patient.domain.Sex;
import com.bwas.openclinic.visit.VisitStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.UUID;

public abstract class AbstractTest {

    @Autowired
    private DataSource dataSource;

    public void clearDb() {
        deleteScheduleEntries();
        deleteSchedules();
        deleteDiagnosis();
        deleteVisit();
        deletePrescriptions();
        deletePatient();
        deleteDoctors();
        deleteUsers();
        deleteClinics();
    }

    public void deleteClinics() {
        new JdbcTemplate(dataSource).execute("DELETE FROM clinic");
    }

    public void deleteDiagnosis() {
        new JdbcTemplate(dataSource).execute("DELETE FROM diagnosis");
    }

    public void deleteUsers() {
        new JdbcTemplate(dataSource).execute("DELETE FROM user");
    }

    public void deleteDoctors() {
        new JdbcTemplate(dataSource).execute("DELETE FROM doctor");
    }

    public void deletePatient() {
        new JdbcTemplate(dataSource).execute("DELETE FROM patient");
    }

    public void deleteVisit() {
        new JdbcTemplate(dataSource).execute("DELETE FROM visit");
    }

    public void deletePrescriptions() {
        new JdbcTemplate(dataSource).execute("DELETE FROM prescription");
    }

    public void deleteSchedules() {
        new JdbcTemplate(dataSource).execute("DELETE FROM schedule");
    }

    public void deleteScheduleEntries() {
        new JdbcTemplate(dataSource).execute("DELETE FROM schedule_entry");
    }

    public void addClinic(int id, UUID clinicUuid, String name, String street, String city, String postalCode, String phoneNumber) {
        new JdbcTemplate(dataSource).update(
                "insert into clinic(id, uuid, name, street, city, postal_code, phone_number)" +
                        "values (" + id + ",'" + clinicUuid + "', '" + name + "', '" + street + "','" + city + "','" + postalCode + "','" + phoneNumber + "')");
    }

    public void addDiagnosis(int id, UUID diagnosisUuid, String name, String description, String date, Long doctorId, Long visitID, Long patientId) {
        new JdbcTemplate(dataSource).update(
                "insert into diagnosis(id, uuid, name, description, date, doctor_id, visit_id, patient_id)" +
                        "values (" + id + ",'" + diagnosisUuid + "', '" + name + "', '" + description + "','" + date + "','" + doctorId + "','" + visitID + "','" + patientId +"')");
    }

    public void addUser(int id, UUID userUuid, String email, String firstName, String lastName, String phoneNumber, Role role, String password, boolean active) {
        new JdbcTemplate(dataSource).update(
                "insert into user(id, uuid, email, first_name, last_name, phone_number, role, password, active)" +
                "values (" + id + ",'" + userUuid + "','" + email + "', '" + firstName + "', '" + lastName + "', '" + phoneNumber + "', '" + role.toString() + "','" + password + "','" + active + "')");
    }

    public void addDoctor(int id, UUID doctorUuid, String email, String firstName, String lastName, String phoneNumber, Role role, String password, boolean active, String description, Specialization specialization) {
        addUser(id, doctorUuid, email, firstName, lastName, phoneNumber, role, password, active);
        new JdbcTemplate(dataSource).update(
                "insert into doctor(id, description, specialization)" +
                "values(" + id + ",'" + description + "','" + specialization + "')");
    }

    public void addPatient(int id, UUID doctorUuid, String email, String firstName, String lastName, String phoneNumber, Role role, String password, boolean active, Sex sex, String identityNumber, String dateOfJoining, Long doctorId) {
        addUser(id, doctorUuid, email, firstName, lastName, phoneNumber, role, password, active);
        new JdbcTemplate(dataSource).update(
                "insert into patient(id, sex, identity_number, date_of_joining, doctor_id)" +
                "values (" + id + ",'" + sex + "','" + identityNumber + "','" + dateOfJoining + "', '" + doctorId + "')");
    }

    public void addVisit(int id, UUID visitUuid, String startDate, String endDate, VisitStatus status, BigDecimal price, String type, Long clinicId, Long patientId, Long doctorId) {
        new JdbcTemplate(dataSource).update(
                "insert into visit(id, uuid, start_date, end_date, status, price, type, clinic_id, patient_id, doctor_id) " +
                        "values (" + id + ",'" + visitUuid + "','" + startDate + "','" + endDate + "','" + status + "','" + price + "','" + type + "','" + clinicId + "','" + patientId + "','" + doctorId + "')");
    }

    public void addPrescription(int id, UUID visitUuid, String code, String dateOfIssuing, String dateOfExpiration, String medicineName, String description, Long patientId, Long doctorId) {
        new JdbcTemplate(dataSource).update(
                "insert into prescription(id, uuid, code, date_of_issuing, date_of_expiration, medicine_name, description, patient_id, doctor_id) " +
                        "values (" + id + ",'" + visitUuid + "','" + code + "','" + dateOfIssuing + "','" + dateOfExpiration + "','" + medicineName + "','" + description + "','" + patientId + "','" + doctorId + "')");
    }

    public void addSchedule(int id, UUID scheduleUuid, String startDate, String endDate, boolean active, Long doctorId, Long clinicId) {
        new JdbcTemplate(dataSource).update(
                "insert into schedule(id, uuid, start_date, end_date, active, doctor_id)" +
                        "values(" + id + ",'" + scheduleUuid + "','" + startDate + "','" + endDate + "','" + active + "','" + doctorId + "')");

        new JdbcTemplate(dataSource).update(
                "insert into schedule_entry(id, uuid, day, start_hour, end_hour, clinic_id, schedule_id)" +
                        "values(7, '71471352-7b43-11ea-bc55-0242ac130003', 'MONDAY', '08:00:00', '17:00:00','" + clinicId + "','" + id + "')");
        new JdbcTemplate(dataSource).update(
                "insert into schedule_entry(id, uuid, day, start_hour, end_hour, clinic_id, schedule_id)" +
                        "values(8, '1582b048-7b44-11ea-bc55-0242ac130003', 'TUESDAY', '08:00:00', '17:00:00', '" + clinicId + "', '" + id + "')");
        new JdbcTemplate(dataSource).update(
                "insert into schedule_entry(id, uuid, day, start_hour, end_hour, clinic_id, schedule_id)" +
                        "values(9, '29fc8a62-7b44-11ea-bc55-0242ac130003', 'WEDNESDAY', '08:00:00', '17:00:00', '" + clinicId + "', '" + id + "')");
        new JdbcTemplate(dataSource).update(
                "insert into schedule_entry(id, uuid, day, start_hour, end_hour, clinic_id, schedule_id)" +
                        "values(10, '65b6efe8-7b44-11ea-bc55-0242ac130003', 'THURSDAY', '08:00:00', '17:00:00', '" + clinicId + "', '" + id + "')");
        new JdbcTemplate(dataSource).update(
                "insert into schedule_entry(id, uuid, day, start_hour, end_hour, clinic_id, schedule_id)" +
                        "values(11, '7f387e96-7b44-11ea-bc55-0242ac130003', 'FRIDAY', '08:00:00', '17:00:00', '" + clinicId + "', '" + id + "')");
    }

}
