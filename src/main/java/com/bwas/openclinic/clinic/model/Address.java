package com.bwas.openclinic.clinic.model;

import lombok.Getter;

import javax.persistence.Embeddable;

@Getter
@Embeddable
public class Address {
    private String street;

    private String city;

    private String postalCode;
}
