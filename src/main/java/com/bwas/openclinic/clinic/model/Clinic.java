package com.bwas.openclinic.clinic.model;

import com.bwas.openclinic.shared.AbstractEntity;
import com.bwas.openclinic.visit.Visit;
import lombok.Getter;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Entity
@Table(name = "clinic")
public class Clinic extends AbstractEntity {

    @Column(nullable = false, unique = true)
    private String name;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "street", column = @Column(nullable = false)),
            @AttributeOverride(name = "city", column = @Column(nullable = false)),
            @AttributeOverride(name = "postalCode", column = @Column(nullable = false))
    })
    private Address address;

    @Column(nullable = false)
    private String phoneNumber;

    @Lob
    @Column(columnDefinition = "BLOB")
    private byte[] photo;

    @Column
    @OneToMany(mappedBy = "clinic")
    private Set<Visit> visits = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Clinic)) return false;
        Clinic clinic = (Clinic) o;
        return Objects.equals(uuid, clinic.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
