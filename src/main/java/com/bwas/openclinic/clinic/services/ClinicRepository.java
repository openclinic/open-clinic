package com.bwas.openclinic.clinic.services;

import com.bwas.openclinic.clinic.model.Clinic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
interface ClinicRepository extends JpaRepository<Clinic,Long> {

    Optional<Clinic> findByUuid(UUID uuid);
}
