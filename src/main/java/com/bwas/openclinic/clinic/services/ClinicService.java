package com.bwas.openclinic.clinic.services;

import com.bwas.openclinic.clinic.model.Clinic;

import java.util.List;
import java.util.UUID;

public interface ClinicService {

    Clinic find(UUID uuid);

    List<Clinic> findAll();

}
