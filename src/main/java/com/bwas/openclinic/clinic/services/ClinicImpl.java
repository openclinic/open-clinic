package com.bwas.openclinic.clinic.services;

import com.bwas.openclinic.clinic.model.Clinic;
import com.bwas.openclinic.exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
class ClinicImpl implements ClinicService {

    private final ClinicRepository clinicRepository;

    ClinicImpl(ClinicRepository clinicRepository) {
        this.clinicRepository = clinicRepository;
    }

    @Override
    public Clinic find(UUID uuid) {
        return clinicRepository.findByUuid(uuid)
                .orElseThrow(()-> new NotFoundException("Clinic with UUID: " + uuid));
    }

    @Override
    public List<Clinic> findAll() {
        return clinicRepository.findAll();
    }
}
