package com.bwas.openclinic.clinic.api;

import com.bwas.openclinic.clinic.model.Address;
import com.bwas.openclinic.clinic.model.Clinic;

class Mapper {

    private Mapper(){}

    static ClinicDTO map(Clinic clinic) {
       return ClinicDTO.builder()
               .uuid(clinic.getUuid())
               .name(clinic.getName())
               .phoneNumber(clinic.getPhoneNumber())
               .photo(clinic.getPhoto())
               .address(map(clinic.getAddress()))
               .build();
    }

    static AddressDTO map(Address address) {
        return AddressDTO.builder()
                .city(address.getCity())
                .postalCode(address.getPostalCode())
                .street(address.getStreet())
                .build();
    }

}
