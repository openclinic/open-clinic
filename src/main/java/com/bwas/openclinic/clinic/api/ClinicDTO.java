package com.bwas.openclinic.clinic.api;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Getter
@Builder
class ClinicDTO {

    private UUID uuid;

    private String name;

    private AddressDTO address;

    private String phoneNumber;

    private byte[] photo ;

}
