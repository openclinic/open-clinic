package com.bwas.openclinic.clinic.api;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
class AddressDTO {

    private String street;

    private String city;

    private String postalCode;

}
