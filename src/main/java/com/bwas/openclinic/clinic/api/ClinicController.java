package com.bwas.openclinic.clinic.api;

import com.bwas.openclinic.clinic.services.ClinicService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/clinics")
public class ClinicController {

    private final ClinicService clinicService;

    public ClinicController(ClinicService clinicService) {
        this.clinicService = clinicService;
    }

    @GetMapping("/{uuid}")
    @PreAuthorize("hasRole('ROLE_PATIENT') or hasRole('ROLE_DOCTOR')")
    public ClinicDTO find(@PathVariable UUID uuid) {
        return Mapper.map(clinicService.find(uuid));
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_PATIENT')")
    public List<ClinicDTO> findAll() {
        return clinicService.findAll().stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }
}
