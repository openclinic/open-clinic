package com.bwas.openclinic.diagnosis.api;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Getter
@Builder
class NewDiagnosisDTO {
    private String name;
    private String description;
    private UUID patientUUID;
    private UUID doctorUUID;
    private UUID visitUUID;
}
