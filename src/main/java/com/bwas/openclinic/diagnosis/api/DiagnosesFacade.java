package com.bwas.openclinic.diagnosis.api;

import com.bwas.openclinic.diagnosis.domain.Diagnosis;
import com.bwas.openclinic.diagnosis.services.DiagnosisService;
import com.bwas.openclinic.shared.DataPage;
import com.bwas.openclinic.user.doctor.domain.Doctor;
import com.bwas.openclinic.user.doctor.services.DoctorService;
import com.bwas.openclinic.user.patient.domain.Patient;
import com.bwas.openclinic.user.patient.services.PatientService;
import com.bwas.openclinic.visit.Visit;
import com.bwas.openclinic.visit.services.VisitService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.UUID;

@Component
class DiagnosesFacade {

    private final DiagnosisService diagnosisService;
    private final DoctorService doctorService;
    private final PatientService patientService;
    private final VisitService visitService;

    public DiagnosesFacade(DiagnosisService diagnosisService, DoctorService doctorService, PatientService patientService, VisitService visitService) {
        this.diagnosisService = diagnosisService;
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.visitService = visitService;
    }

    DataPage<DiagnosisDTO> findAll(Pageable page, UUID patientUUID) {
        Page<Diagnosis> diagnosisPage = diagnosisService.find(page, patientUUID);
        return Mapper
                .map(diagnosisPage, page.getPageNumber(), page.getPageSize());
    }

    DiagnosisDTO create(NewDiagnosisDTO newDiagnosisDTO) {
        String name = newDiagnosisDTO.getName();
        String description = newDiagnosisDTO.getDescription();
        LocalDate date = LocalDate.now();

        Doctor doctor = doctorService.findByUUID(newDiagnosisDTO.getDoctorUUID());
        Patient patient = patientService.findByUUID(newDiagnosisDTO.getPatientUUID());
        Visit visit = visitService.find(newDiagnosisDTO.getVisitUUID());

        Diagnosis diagnosis = new Diagnosis(
                name, description, date, patient, doctor, visit
        );

        return Mapper.map(diagnosisService.create(diagnosis));
    }
}
