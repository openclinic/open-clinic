package com.bwas.openclinic.diagnosis.api;

import com.bwas.openclinic.diagnosis.domain.Diagnosis;
import com.bwas.openclinic.shared.DataPage;
import org.springframework.data.domain.Page;

import java.util.stream.Collectors;

class Mapper {

    private Mapper(){}

    static DataPage<DiagnosisDTO> map(Page<Diagnosis> diagnosisPage, Integer pageNumber, Integer pageSize) {
        return DataPage.<DiagnosisDTO>builder()
                .content(diagnosisPage.stream()
                        .map(Mapper::map)
                        .collect(Collectors.toList()))
                .number(pageNumber)
                .size(pageSize)
                .totalElements(diagnosisPage.getTotalElements())
                .build();
    }

    static DiagnosisDTO map(Diagnosis diagnosis) {
        return DiagnosisDTO.builder()
                .uuid(diagnosis.getUuid())
                .name(diagnosis.getName())
                .description(diagnosis.getDescription())
                .date(diagnosis.getDate())
                .doctorUUID(diagnosis.getDoctor().getUuid())
                .visitUUID(diagnosis.getVisit().getUuid())
                .patientUUID(diagnosis.getPatient().getUuid())
                .build();
    }
}
