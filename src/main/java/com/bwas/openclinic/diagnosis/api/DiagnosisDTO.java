package com.bwas.openclinic.diagnosis.api;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;
import java.util.UUID;

@Builder
@Getter
class DiagnosisDTO {

    private UUID uuid;

    private String name;

    private String description;

    private LocalDate date;

    private UUID doctorUUID;

    private UUID visitUUID;

    private UUID patientUUID;

}
