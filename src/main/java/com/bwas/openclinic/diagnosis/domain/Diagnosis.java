package com.bwas.openclinic.diagnosis.domain;

import com.bwas.openclinic.shared.AbstractEntity;
import com.bwas.openclinic.user.doctor.domain.Doctor;
import com.bwas.openclinic.user.patient.domain.Patient;
import com.bwas.openclinic.visit.Visit;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;

@Getter
@Entity
@Table(name = "diagnosis")
public class Diagnosis extends AbstractEntity {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private LocalDate date;

    @ManyToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @ManyToOne
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    @ManyToOne
    @JoinColumn(name = "visit_id")
    private Visit visit;

    public Diagnosis(){}

    public Diagnosis(String name, String description, LocalDate date, Patient patient, Doctor doctor, Visit visit) {
        this.name = name;
        this.description = description;
        this.date = date;
        this.patient = patient;
        this.doctor = doctor;
        this.visit = visit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Diagnosis)) return false;
        Diagnosis diagnosis = (Diagnosis) o;
        return Objects.equals(uuid, diagnosis.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }

}
