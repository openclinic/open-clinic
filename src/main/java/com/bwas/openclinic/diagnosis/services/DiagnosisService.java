package com.bwas.openclinic.diagnosis.services;

import com.bwas.openclinic.diagnosis.domain.Diagnosis;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface DiagnosisService {

    Page<Diagnosis> find(Pageable page, UUID patientUUID);

    Diagnosis create(Diagnosis diagnosis);

}
