package com.bwas.openclinic.diagnosis.services;

import com.bwas.openclinic.diagnosis.domain.Diagnosis;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
interface DiagnosisRepository extends JpaRepository<Diagnosis, Long> {
    Page<Diagnosis> findByPatientUuid(UUID patientUUID, Pageable pageable);
}
