package com.bwas.openclinic.diagnosis.services;

import com.bwas.openclinic.diagnosis.domain.Diagnosis;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
class DiagnosisServiceImpl implements DiagnosisService {

    private final DiagnosisRepository diagnosisRepository;

    public DiagnosisServiceImpl(DiagnosisRepository diagnosisRepository) {
        this.diagnosisRepository = diagnosisRepository;
    }

    @Override
    public Page<Diagnosis> find(Pageable page, UUID patientUUID) {
        return diagnosisRepository.findByPatientUuid(patientUUID, page);
    }

    @Override
    public Diagnosis create(Diagnosis diagnosis) {
        return diagnosisRepository.save(diagnosis);
    }
}
