package com.bwas.openclinic.security;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String UUID_CLAIM = "uuid";
    public static final String EMAIL_CLAIM = "email";
    public static final String ROLE_CLAIM = "role";
    public static final String FIRST_NAME_CLAIM = "first_name";
    public static final String LAST_NAME_CLAIM = "last_name";
    public static final String PHONE_NUMBER_CLAIM= "phone_number";


    private AuthenticationManager authenticationManager;
    private JwtProperties jwtProperties;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager, JwtProperties jwtProperties) {
        this.authenticationManager = authenticationManager;
        this.jwtProperties = jwtProperties;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {

        UserCredentials credentials = new UserCredentials();
        try {
            credentials = new ObjectMapper().readValue(request.getInputStream(), UserCredentials.class);
        } catch (IOException e) {
        }

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                credentials.getEmail(),
                credentials.getPassword(),
                new ArrayList<>());

        return authenticationManager.authenticate(authenticationToken);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        UserPrincipal principal = (UserPrincipal) authResult.getPrincipal();

        String token = JWT.create()
                .withSubject(principal.getUsername())
                .withClaim(UUID_CLAIM, principal.getUuid().toString())
                .withClaim(EMAIL_CLAIM, principal.getUsername())
                .withClaim(ROLE_CLAIM, principal.getRole().getName())
                .withClaim(FIRST_NAME_CLAIM, principal.getFirstName())
                .withClaim(LAST_NAME_CLAIM, principal.getLastName())
                .withClaim(PHONE_NUMBER_CLAIM, principal.getPhoneNumber())
                .withExpiresAt(new Date(System.currentTimeMillis() + Integer.parseInt(jwtProperties.getExpirationTime())))
                .sign(HMAC512(jwtProperties.getSecret().getBytes()));

        response.addHeader(HttpHeaders.AUTHORIZATION, TOKEN_PREFIX +  token);
    }
}
