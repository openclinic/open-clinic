package com.bwas.openclinic.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.bwas.openclinic.user.domain.entity.User;
import com.bwas.openclinic.user.services.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {
    public static final String TOKEN_PREFIX = "Bearer ";

    private UserService userService;
    private final JwtProperties jwtProperties;

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager, UserService userService, JwtProperties jwtProperties) {
        super(authenticationManager);
        this.userService = userService;
        this.jwtProperties = jwtProperties;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(HttpHeaders.AUTHORIZATION);

        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(request, response);
            return;
        }

        Authentication authentication = getUsernamePasswordAuthentication(request);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        chain.doFilter(request, response);
    }

    private Authentication getUsernamePasswordAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);

        if (token != null) {
            DecodedJWT decodedJWT = JWT.require(HMAC512(jwtProperties.getSecret().getBytes()))
                    .build()
                    .verify(token.replace(TOKEN_PREFIX, ""));
            String userUUID = decodedJWT.getClaim("uuid").asString();
            String userName = decodedJWT.getSubject();


            if (userUUID != null) {
                User user = userService.find(UUID.fromString(userUUID));
                UserPrincipal principal = new UserPrincipal(user);
                return new UsernamePasswordAuthenticationToken(userName, null, principal.getAuthorities());
            }
            return null;
        }
        return null;
    }
}
