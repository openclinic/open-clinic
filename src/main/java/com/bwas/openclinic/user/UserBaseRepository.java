package com.bwas.openclinic.user;

import com.bwas.openclinic.user.domain.entity.User;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@NoRepositoryBean
public interface UserBaseRepository<T extends User>
        extends Repository<T, Long> {

    Optional<T> findByUuid(UUID uuid);

    Optional<T> findByEmail(String email);

    List<T> findAll();
}
