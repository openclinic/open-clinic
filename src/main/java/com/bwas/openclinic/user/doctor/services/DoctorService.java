package com.bwas.openclinic.user.doctor.services;

import com.bwas.openclinic.user.doctor.domain.Doctor;
import com.bwas.openclinic.user.doctor.domain.Specialization;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface DoctorService {

    Doctor find(UUID patientUUID);

    Doctor findByUUID(UUID uuid);

    List<String> findAllSpecializations();

    List<Doctor> findAll(Specialization specialization);

    List<Doctor> findAll();

    List<Doctor> findAll(LocalDateTime date, Specialization specialization, UUID clinicUUOD);

}
