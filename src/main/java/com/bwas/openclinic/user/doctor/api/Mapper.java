package com.bwas.openclinic.user.doctor.api;

import com.bwas.openclinic.user.doctor.domain.Doctor;

class Mapper {

    private Mapper(){}

    static DoctorDTO map(Doctor doctor) {
       return DoctorDTO.builder()
               .uuid(doctor.getUuid())
               .firstName(doctor.getFirstName())
               .lastName(doctor.getLastName())
               .email(doctor.getEmail())
               .phoneNumber(doctor.getPhoneNumber())
               .specialization(doctor.getSpecialization())
               .description(doctor.getDescription())
               .photo(doctor.getPhoto())
               .build();
    }

}
