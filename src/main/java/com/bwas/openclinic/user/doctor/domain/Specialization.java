package com.bwas.openclinic.user.doctor.domain;

import java.math.BigDecimal;

public enum  Specialization {
    SURGERY("Surgery", new BigDecimal(2000)),
    CARDIOLOGY("Cardiology", new BigDecimal(5000)),
    NEUROSURGERY("Neurosurgery", new BigDecimal(200)),
    ANESTHESIOLOGY("Anesthesiology", new BigDecimal(2000)),
    DERMATOLOGY("Dermatology", new BigDecimal(2000)),
    FAMILY_MEDICINE("Family medicine", new BigDecimal(2000)),
    IMMUNOLOGY("Immunology", new BigDecimal(2000)),
    GYNECOLOGY("Gynecology", new BigDecimal(2000)),
    PEDIATRICS("Pediatrics", new BigDecimal(2000)),
    PHYSICAL_MEDICINE("Physical medicine", new BigDecimal(2000));

    private final String name;
    private final BigDecimal price;

    Specialization(String name, BigDecimal price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return this.name;
    }

    public BigDecimal getPrice() {
        return this.price;
    }
}
