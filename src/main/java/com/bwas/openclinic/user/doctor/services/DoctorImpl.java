package com.bwas.openclinic.user.doctor.services;

import com.bwas.openclinic.exceptions.NotFoundException;
import com.bwas.openclinic.schedule.Schedule;
import com.bwas.openclinic.schedule.ScheduleEntry;
import com.bwas.openclinic.user.doctor.domain.Doctor;
import com.bwas.openclinic.user.doctor.domain.Specialization;
import com.bwas.openclinic.visit.services.VisitService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
class DoctorImpl implements DoctorService {

    @Value("${visit.length}")
    private long visitLength;

    private final DoctorRepository doctorRepository;
    private final VisitService visitService;

    DoctorImpl(DoctorRepository doctorRepository, VisitService visitService) {
        this.doctorRepository = doctorRepository;
        this.visitService = visitService;
    }

    @Override
    public Doctor find(UUID patientUUID) {
        return doctorRepository.findByPatientsUuid(patientUUID)
                .orElseThrow(() -> new NotFoundException("Doctor with patient's UUID: " + patientUUID));
    }

    @Override
    public Doctor findByUUID(UUID uuid) {
        return doctorRepository.findByUuid(uuid)
                .orElseThrow(() -> new NotFoundException("Doctor with UUID: " + uuid));
    }

    @Override
    public List<String> findAllSpecializations() {
        return Arrays.stream(Specialization.values())
                .map(Specialization::getName)
                .collect(Collectors.toList());
    }

    @Override
    public List<Doctor> findAll(Specialization specialization) {
        return doctorRepository.findBySpecialization(specialization);
    }

    @Override
    public List<Doctor> findAll() {
        return doctorRepository.findAll();
    }

    @Override
    @Transactional
    public List<Doctor> findAll(LocalDateTime date, Specialization specialization, UUID clinicUUID) {
        return doctorRepository.findBySpecializationAndSchedulesActiveAndSchedulesStartDateLessThanEqual(specialization, true, date.toLocalDate()).stream()
                .filter(doctor -> isAvailable(doctor, date, clinicUUID))
                .filter(doctor -> isUnoccupied(doctor, date))
                .collect(Collectors.toList());
    }

    private boolean isAvailable(Doctor doctor, LocalDateTime date, UUID clinicUUID) {
        DayOfWeek dayOfVisit = date.getDayOfWeek();
        LocalTime visitStart = date.toLocalTime();
        Optional<Schedule> currentSchedule = getCurrent(doctor);
        if(currentSchedule.isPresent()) {
            Optional<ScheduleEntry> scheduleDay = getScheduleDay(currentSchedule.get(), dayOfVisit, clinicUUID);
            return scheduleDay
                    .filter(scheduleEntry -> fitsScheduleDay(scheduleEntry, visitStart))
                    .isPresent();
        }
        return false;
    }

    private Optional<Schedule> getCurrent(Doctor doctor) {
        return doctor.getSchedules().stream()
                .filter(Schedule::isActive)
                .findFirst();
    }

    private Optional<ScheduleEntry> getScheduleDay(Schedule schedule, DayOfWeek dayOfVisit, UUID clinicUUID) {
        return schedule.getScheduleEntries().stream()
                .filter(scheduleEntry -> scheduleEntry.getDay().equals(dayOfVisit) && scheduleEntry.getClinic().getUuid().equals(clinicUUID))
                .findFirst();
    }

    private boolean fitsScheduleDay(ScheduleEntry scheduleDay, LocalTime startHour) {
        if(scheduleDay == null) {
            return false;
        }
        LocalTime endHour = startHour.plusMinutes(this.visitLength);
        return scheduleDay.getStartHour().minusMinutes(1).isBefore(startHour) && scheduleDay.getEndHour().plusMinutes(1).isAfter(endHour);
    }

    private boolean isUnoccupied(Doctor doctor, LocalDateTime date) {
          return  visitService
                    .find(doctor.getUuid(), date, date.plusMinutes(this.visitLength)).isEmpty();
    }
}
