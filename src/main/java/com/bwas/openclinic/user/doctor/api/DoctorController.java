package com.bwas.openclinic.user.doctor.api;

import com.bwas.openclinic.user.doctor.domain.Specialization;
import com.bwas.openclinic.user.doctor.services.DoctorService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/doctors")
public class DoctorController {

    private final DoctorService doctorService;

    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping(params = {"patientUUID"})
    @PreAuthorize("hasRole('ROLE_PATIENT')")
    public DoctorDTO find(@RequestParam UUID patientUUID) {
        return Mapper.map(doctorService.find(patientUUID));
    }

    @GetMapping("/{uuid}")
    @PreAuthorize("hasRole('ROLE_PATIENT') or hasRole('ROLE_DOCTOR')")
    public DoctorDTO findByUUID(@PathVariable UUID uuid) {
        return Mapper.map(doctorService.findByUUID(uuid));
    }

    @GetMapping("/specializations")
    @PreAuthorize("hasRole('ROLE_PATIENT')")
    public List<String> findAllSpecializations() {
        return doctorService.findAllSpecializations();
    }

    @GetMapping(params = {"specialization"})
    @PreAuthorize("hasRole('ROLE_PATIENT')")
    public List<DoctorDTO> findAll(@RequestParam Specialization specialization) {
        return doctorService.findAll(specialization).stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_PATIENT')")
    public List<DoctorDTO> findAll() {
        return doctorService.findAll().stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }

    @GetMapping(params = {"date", "specialization", "clinicUUID"})
    @PreAuthorize("hasRole('ROLE_PATIENT')")
    public List<DoctorDTO> findAll(@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") @RequestParam LocalDateTime date,
                                   @RequestParam Specialization specialization,
                                   @RequestParam UUID clinicUUID) {
        return doctorService.findAll(date, specialization, clinicUUID).stream()
                .map(Mapper::map)
                .collect(Collectors.toList());

    }
}
