package com.bwas.openclinic.user.doctor.domain;

import com.bwas.openclinic.diagnosis.domain.Diagnosis;
import com.bwas.openclinic.prescription.domain.Prescription;
import com.bwas.openclinic.schedule.Schedule;
import com.bwas.openclinic.user.domain.entity.Role;
import com.bwas.openclinic.user.domain.entity.User;
import com.bwas.openclinic.user.patient.domain.Patient;
import com.bwas.openclinic.visit.Visit;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Entity
@Table(name = "doctor")
public class Doctor extends User {

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Specialization specialization;

    private String description;

    @OneToMany(mappedBy = "doctor")
    private Set<Schedule> schedules = new HashSet<>();

    @OneToMany(mappedBy = "doctor")
    private Set<Patient> patients = new HashSet<>();

    @OneToMany(mappedBy = "doctor")
    private Set<Visit> visits = new HashSet<>();

    @OneToMany(mappedBy = "doctor")
    private Set<Prescription> prescriptions = new HashSet<>();

    @OneToMany(mappedBy = "doctor")
    private Set<Diagnosis> diagnoses = new HashSet<>();

    public Doctor(String email, String firstName, String lastName, String phoneNumber, String password, Role role, boolean active, Specialization specialization, String description) {
        super(email, firstName, lastName, phoneNumber, role, active, password);
        this.specialization = specialization;
        this.description = description;
    }

    public Doctor(){}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Doctor)) return false;
        Doctor doctor = (Doctor) o;
        return Objects.equals(uuid, doctor.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
