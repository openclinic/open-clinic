package com.bwas.openclinic.user.doctor.services;

import com.bwas.openclinic.user.UserBaseRepository;
import com.bwas.openclinic.user.doctor.domain.Doctor;
import com.bwas.openclinic.user.doctor.domain.Specialization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
interface DoctorRepository extends UserBaseRepository<Doctor>, JpaRepository<Doctor,Long> {

    Optional<Doctor> findByPatientsUuid(UUID patientUUID);

    List<Doctor> findBySpecialization(Specialization specialization);

    List<Doctor> findBySpecializationAndSchedulesActiveAndSchedulesStartDateLessThanEqual(Specialization specialization, boolean isActive, LocalDate date);

}
