package com.bwas.openclinic.user.doctor.api;

import com.bwas.openclinic.user.doctor.domain.Specialization;
import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Getter
@Builder
class DoctorDTO {

    private UUID uuid;

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

    private Specialization specialization;

    private String description;

    private byte[] photo;

}
