package com.bwas.openclinic.user.api;

import com.bwas.openclinic.user.services.UserService;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@RestController("/users")
public class UserController {

    private final Mapper mapper;
    private final UserService userService;

    public UserController(Mapper mapper, UserService userService) {
        this.mapper = mapper;
        this.userService = userService;
    }

    @PatchMapping("/{uuid}/photo")
    public void uploadImage(@PathVariable UUID uuid, @RequestParam("imageFile") MultipartFile file) throws IOException {
        userService.update(uuid, file);
    }

    @PatchMapping("/{uuid}")
    public UserDTO update(@PathVariable UUID uuid, @RequestBody UserUpdateDTO userUpdateDTO) {
        return mapper.map(userService.update(uuid, mapper.map(userUpdateDTO)));
    }

}
