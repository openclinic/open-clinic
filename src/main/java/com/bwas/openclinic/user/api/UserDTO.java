package com.bwas.openclinic.user.api;

import com.bwas.openclinic.user.domain.entity.Role;
import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Getter
@Builder
class UserDTO {

    private UUID uuid;

    private String email;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private Role role;
}
