package com.bwas.openclinic.user.api;

import com.bwas.openclinic.user.domain.UserUpdate;
import com.bwas.openclinic.user.domain.entity.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
class Mapper {

    private final PasswordEncoder passwordEncoder;

    public Mapper(PasswordEncoder passwordEncoder){
        this.passwordEncoder = passwordEncoder;
    }

    UserDTO map(User user) {
        return UserDTO.builder()
                .uuid(user.getUuid())
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .phoneNumber(user.getPhoneNumber())
                .role(user.getRole())
                .build();
    }

    UserUpdate map(UserUpdateDTO userUpdateDTO) {
        String password = userUpdateDTO.getPassword();
        return UserUpdate.builder()
                .password(password == null || password.isEmpty() ? "" : passwordEncoder.encode(password))
                .email(userUpdateDTO.getEmail())
                .phoneNumber(userUpdateDTO.getPhoneNumber())
                .build();
    }

}
