package com.bwas.openclinic.user.api;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
class UserUpdateDTO {
    private String email;

    private String phoneNumber;

    private String password;
}
