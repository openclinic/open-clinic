package com.bwas.openclinic.user.domain;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class UserUpdate {

    private String email;

    private String phoneNumber;

    private String password;
}
