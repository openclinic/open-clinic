package com.bwas.openclinic.user.services;

import com.bwas.openclinic.user.domain.entity.User;
import com.bwas.openclinic.user.UserBaseRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface UserRepository extends UserBaseRepository<User>, JpaRepository<User,Long> {
}
