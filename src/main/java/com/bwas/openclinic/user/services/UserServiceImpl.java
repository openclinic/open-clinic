package com.bwas.openclinic.user.services;

import com.bwas.openclinic.exceptions.NotFoundException;
import com.bwas.openclinic.user.domain.entity.User;
import com.bwas.openclinic.user.domain.UserUpdate;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@Service
class UserServiceImpl implements UserService {

    private static final String USER_UUID_EXCEPTION_MESSAGE = "User with UUID: ";
    private final UserRepository userRepository;

    UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User find(UUID uuid) {
        return userRepository.findByUuid(uuid)
                .orElseThrow(() -> new NotFoundException(USER_UUID_EXCEPTION_MESSAGE + uuid));
    }

    @Override
    public User update(UUID uuid, MultipartFile file) throws IOException {
        byte[] photo = file.getBytes();
        return userRepository.findByUuid(uuid)
                .map(user -> {
                    user.setPhoto(photo);
                    return userRepository.save(user);
                })
                .orElseThrow(() -> new NotFoundException(USER_UUID_EXCEPTION_MESSAGE + uuid));
    }

    @Override
    public User update(UUID uuid, UserUpdate userUpdate) {
        return userRepository.findByUuid(uuid)
                .map(user -> {
                    user.setEmail(userUpdate.getEmail());
                    user.setPassword(userUpdate.getPassword());
                    user.setPhoneNumber(userUpdate.getPhoneNumber());
                    return userRepository.save(user);
                })
                .orElseThrow(() -> new NotFoundException(USER_UUID_EXCEPTION_MESSAGE + uuid));
    }

}
