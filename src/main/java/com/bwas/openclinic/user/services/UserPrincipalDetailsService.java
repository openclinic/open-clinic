package com.bwas.openclinic.user.services;

import com.bwas.openclinic.security.UserPrincipal;
import com.bwas.openclinic.user.domain.entity.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
class UserPrincipalDetailsService implements UserDetailsService {
    private UserRepository userRepository;

    public UserPrincipalDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) {
        User user = this.userRepository.findByEmail(s)
                .orElseThrow(() ->new UsernameNotFoundException("User Not Found"));

        return new UserPrincipal(user);
    }
}
