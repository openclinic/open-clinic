package com.bwas.openclinic.user.services;

import com.bwas.openclinic.user.domain.entity.User;
import com.bwas.openclinic.user.domain.UserUpdate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

public interface UserService {

    User find(UUID uuid);

    User update(UUID uuid, MultipartFile file) throws IOException;

    User update(UUID uuid, UserUpdate userUpdate);

}
