package com.bwas.openclinic.user.patient.services;

import com.bwas.openclinic.user.patient.domain.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface PatientService {

    Patient findByUUID(UUID uuid);

    Page<Patient> findAll(Pageable page, UUID doctorUUID);

}
