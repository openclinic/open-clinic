package com.bwas.openclinic.user.patient.domain;

public enum Sex {

    FEMALE("Female"),
    MALE("Male");

    private final String name;

    Sex(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

}
