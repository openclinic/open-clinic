package com.bwas.openclinic.user.patient.domain;

import com.bwas.openclinic.diagnosis.domain.Diagnosis;
import com.bwas.openclinic.prescription.domain.Prescription;
import com.bwas.openclinic.user.domain.entity.User;
import com.bwas.openclinic.user.doctor.domain.Doctor;
import com.bwas.openclinic.visit.Visit;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Entity
@Table(name = "patient")
public class Patient extends User {

    @Column(nullable = false)
    private LocalDate dateOfJoining;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @Column(nullable = false, unique = true)
    private String identityNumber;

    @ManyToOne
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    @OneToMany(mappedBy = "patient")
    private Set<Visit> visits = new HashSet<>();

    @OneToMany(mappedBy = "patient")
    private Set<Prescription> prescriptions = new HashSet<>();

    @OneToMany(mappedBy = "patient")
    private Set<Diagnosis> diagnoses = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;
        Patient patient = (Patient) o;
        return Objects.equals(uuid, patient.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
