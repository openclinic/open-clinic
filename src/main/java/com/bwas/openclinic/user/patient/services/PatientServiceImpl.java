package com.bwas.openclinic.user.patient.services;

import com.bwas.openclinic.exceptions.NotFoundException;
import com.bwas.openclinic.user.patient.domain.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
class PatientServiceImpl implements PatientService {

    private final PatientRepository patientRepository;

    public PatientServiceImpl(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    @Override
    public Patient findByUUID(UUID uuid) {
        return patientRepository.findByUuid(uuid)
                .orElseThrow(() -> new NotFoundException("Patient with UUID: " + uuid));
    }

    @Override
    public Page<Patient> findAll(Pageable page, UUID doctorUUID) {
        return patientRepository.findByDoctorUuid(doctorUUID, page);
    }
}
