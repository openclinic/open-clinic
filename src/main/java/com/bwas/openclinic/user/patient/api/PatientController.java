package com.bwas.openclinic.user.patient.api;

import com.bwas.openclinic.shared.DataPage;
import com.bwas.openclinic.user.patient.domain.Patient;
import com.bwas.openclinic.user.patient.services.PatientService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/patients")
public class PatientController {
    private static final String DEFAULT_PAGE_NUMBER = "0";
    private static final String DEFAULT_PAGE_SIZE = "20";
    private static final String DEFAULT_SORT_FIELD = "startDate";
    private static final String DEFAULT_DIRECTION = "ASC";

    private final PatientService patientService;

    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping("/{uuid}")
    @PreAuthorize("hasRole('ROLE_PATIENT') or hasRole('ROLE_DOCTOR')")
    public PatientDTO find(@PathVariable UUID uuid) {
        return Mapper.map(patientService.findByUUID(uuid));
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_DOCTOR')")
    public DataPage<PatientDTO> findAll(@RequestParam(defaultValue =  DEFAULT_PAGE_NUMBER) Integer pageNumber,
                                      @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) Integer pageSize,
                                      @RequestParam(defaultValue = DEFAULT_SORT_FIELD) String sortField,
                                      @RequestParam(defaultValue = DEFAULT_DIRECTION) Sort.Direction direction,
                                      @RequestParam UUID doctorUUID) {
        Pageable page = PageRequest
                .of(pageNumber, pageSize, Sort.by(direction, sortField));
        Page<Patient> patientsPage =  patientService.findAll(page, doctorUUID);
        return Mapper
                .map(patientsPage, pageNumber, pageSize);
    }
}
