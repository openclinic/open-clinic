package com.bwas.openclinic.user.patient.api;

import com.bwas.openclinic.shared.DataPage;
import com.bwas.openclinic.user.patient.domain.Patient;
import org.springframework.data.domain.Page;

import java.util.stream.Collectors;

class Mapper {

    private Mapper(){}

    static DataPage<PatientDTO> map(Page<Patient> patientsPage, Integer pageNumber, Integer pageSize) {
        return DataPage.<PatientDTO>builder()
                .content(patientsPage.stream()
                        .map(Mapper::map)
                        .collect(Collectors.toList()))
                .number(pageNumber)
                .size(pageSize)
                .totalElements(patientsPage.getTotalElements())
                .build();
    }

    static PatientDTO map(Patient patient) {
        return PatientDTO.builder()
                .uuid(patient.getUuid())
                .firstName(patient.getFirstName())
                .lastName(patient.getLastName())
                .email(patient.getEmail())
                .dateOfJoining(patient.getDateOfJoining())
                .sex(patient.getSex())
                .identityNumber(patient.getIdentityNumber())
                .phoneNumber(patient.getPhoneNumber())
                .photo(patient.getPhoto())
                .build();
    }
}
