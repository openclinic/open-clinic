package com.bwas.openclinic.user.patient.services;

import com.bwas.openclinic.user.UserBaseRepository;
import com.bwas.openclinic.user.patient.domain.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
interface PatientRepository extends UserBaseRepository<Patient>, JpaRepository<Patient, Long> {
    Page<Patient> findByDoctorUuid(UUID patientUUID, Pageable pageable);
}
