package com.bwas.openclinic.user.patient.api;

import com.bwas.openclinic.user.patient.domain.Sex;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Builder
class PatientDTO {

    private UUID uuid;

    private String firstName;

    private String lastName;

    private String email;

    private LocalDate dateOfJoining;

    private Sex sex;

    private String identityNumber;

    private String phoneNumber;

    private byte[] photo;

}
