package com.bwas.openclinic.exceptions;

public class NoScheduleEntryException extends RuntimeException {

    public NoScheduleEntryException() {
        super("Given schedule has no day registered for a given clinic");
    }
}
