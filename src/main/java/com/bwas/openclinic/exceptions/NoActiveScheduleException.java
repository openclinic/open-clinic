package com.bwas.openclinic.exceptions;

public class NoActiveScheduleException extends RuntimeException {

    public NoActiveScheduleException() {
        super("Doctor has no active schedules");
    }
}
