package com.bwas.openclinic.exceptions;

import java.util.UUID;

public class DoctorAlreadyBooked extends RuntimeException {

    public DoctorAlreadyBooked(UUID uuid) {
        super("Doctor with UUID: " + uuid + "already has a visit at a given time" );
    }
}
