package com.bwas.openclinic.schedule;

import com.bwas.openclinic.prescription.domain.Prescription;
import com.bwas.openclinic.shared.AbstractEntity;
import com.bwas.openclinic.user.doctor.domain.Doctor;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Table(name = "schedule")
public class Schedule  extends AbstractEntity {

    @Column(nullable = false)
    private LocalDate startDate;

    private LocalDate endDate;

    private boolean active;

    @ManyToOne
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    @OneToMany(mappedBy = "schedule")
    private Set<ScheduleEntry> scheduleEntries = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Prescription)) return false;
        Schedule that = (Schedule) o;
        return Objects.equals(uuid, that.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
