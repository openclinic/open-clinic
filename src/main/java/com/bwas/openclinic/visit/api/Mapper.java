package com.bwas.openclinic.visit.api;

import com.bwas.openclinic.clinic.model.Clinic;
import com.bwas.openclinic.shared.DataPage;
import com.bwas.openclinic.user.doctor.domain.Doctor;
import com.bwas.openclinic.user.patient.domain.Patient;
import com.bwas.openclinic.visit.Visit;
import org.springframework.data.domain.Page;

import java.util.stream.Collectors;

class Mapper {

    private Mapper(){}

    static DataPage<VisitDTO> map(Page<Visit> visitPage, Integer pageNumber, Integer pageSize) {
       return DataPage.<VisitDTO>builder()
                .content(visitPage.stream()
                        .map(Mapper::map)
                        .collect(Collectors.toList()))
                .number(pageNumber)
                .size(pageSize)
                .totalElements(visitPage.getTotalElements())
                .build();
    }

    static VisitDTO map(Visit visit) {
        return VisitDTO.builder()
                .uuid(visit.getUuid())
                .startDate(visit.getStartDate())
                .endDate(visit.getEndDate())
                .price(visit.getPrice())
                .type(visit.getType())
                .doctor(map(visit.getDoctor()))
                .patient(map(visit.getPatient()))
                .description(visit.getType())
                .clinic(map(visit.getClinic()))
                .status(visit.getStatus())
                .build();
    }

    static DoctorDTO map(Doctor doctor) {
        return DoctorDTO.builder()
                .uuid(doctor.getUuid())
                .firstName(doctor.getFirstName())
                .lastName(doctor.getLastName())
                .build();
    }

    static ClinicDTO map(Clinic clinic) {
        return ClinicDTO.builder()
                .uuid(clinic.getUuid())
                .name(clinic.getName())
                .build();
    }

    static PatientDTO map(Patient patient) {
        return PatientDTO.builder()
                .uuid(patient.getUuid())
                .firstName(patient.getFirstName())
                .lastName(patient.getLastName())
                .build();
    }

}
