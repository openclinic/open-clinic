package com.bwas.openclinic.visit.api;

import com.bwas.openclinic.shared.DataPage;
import com.bwas.openclinic.visit.VisitStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/visits")
public class VisitController {
    private static final String DEFAULT_PAGE_NUMBER = "0";
    private static final String DEFAULT_PAGE_SIZE = "20";
    private static final String DEFAULT_SORT_FIELD = "startDate";
    private static final String DEFAULT_DIRECTION = "ASC";

    private final VisitFacade visitFacade;

    public VisitController(VisitFacade visitFacade) {
        this.visitFacade = visitFacade;
    }

    @GetMapping("/all")
    @PreAuthorize("hasRole('ROLE_PATIENT')")
    public DataPage<VisitDTO> findAll(@RequestParam(defaultValue =  DEFAULT_PAGE_NUMBER) Integer pageNumber,
                                      @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) Integer pageSize,
                                      @RequestParam(defaultValue = DEFAULT_SORT_FIELD) String sortField,
                                      @RequestParam(defaultValue = DEFAULT_DIRECTION) Sort.Direction direction,
                                      @RequestParam UUID patientUUID) {
        Pageable page = PageRequest
                .of(pageNumber, pageSize, Sort.by(direction, sortField));
        return visitFacade.findAll(page, patientUUID);
    }

    @GetMapping(params = { "patientUUID" })
    @PreAuthorize("hasRole('ROLE_PATIENT')")
    public DataPage<VisitDTO> findAllWithStatus(@RequestParam(defaultValue =  DEFAULT_PAGE_NUMBER) Integer pageNumber,
                                   @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) Integer pageSize,
                                   @RequestParam(defaultValue = DEFAULT_SORT_FIELD) String sortField,
                                   @RequestParam(defaultValue = DEFAULT_DIRECTION) Sort.Direction direction,
                                   @RequestParam UUID patientUUID,
                                   @RequestParam VisitStatus status) {
        Pageable page = PageRequest
                .of(pageNumber, pageSize, Sort.by(direction, sortField));
        return visitFacade.findAllWithStatus(page, patientUUID, status);
    }

    @GetMapping("/{uuid}")
    @PreAuthorize("hasRole('ROLE_PATIENT') or hasRole('ROLE_DOCTOR')")
    public VisitDTO find(@PathVariable UUID uuid) {
        return visitFacade.find(uuid);
    }

    @PatchMapping("/{uuid}")
    @PreAuthorize("hasRole('ROLE_PATIENT') or hasRole('ROLE_DOCTOR')")
    public VisitDTO update(@PathVariable UUID uuid, @RequestParam VisitStatus status) {
        return visitFacade.update(uuid, status);
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_PATIENT')")
    public VisitDTO create(@RequestBody NewVisitDTO newVisitDTO) {
        return visitFacade.create(newVisitDTO);
    }

}
