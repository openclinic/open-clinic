package com.bwas.openclinic.visit.api;

import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Builder
class NewVisitDTO {

    @NotNull
    private UUID patientUUID;

    @NotNull
    private UUID doctorUUID;

    @NotNull
    private UUID clinicUUID;

    @NotNull
    @FutureOrPresent
    private LocalDateTime startDate;
}
