package com.bwas.openclinic.visit.api;

import com.bwas.openclinic.visit.VisitStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Builder
class VisitDTO {

    private UUID uuid;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime startDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime endDate;

    private String description;

    private BigDecimal price;

    private String type;

    private DoctorDTO doctor;

    private PatientDTO patient;

    private ClinicDTO clinic;

    private VisitStatus status;

}
