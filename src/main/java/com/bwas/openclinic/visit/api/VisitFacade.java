package com.bwas.openclinic.visit.api;

import com.bwas.openclinic.clinic.model.Clinic;
import com.bwas.openclinic.clinic.services.ClinicService;
import com.bwas.openclinic.shared.DataPage;
import com.bwas.openclinic.user.doctor.domain.Doctor;
import com.bwas.openclinic.user.doctor.services.DoctorService;
import com.bwas.openclinic.user.patient.domain.Patient;
import com.bwas.openclinic.user.patient.services.PatientService;
import com.bwas.openclinic.visit.Visit;
import com.bwas.openclinic.visit.VisitStatus;
import com.bwas.openclinic.visit.services.VisitService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.UUID;

@Component
class VisitFacade {

    private final VisitService visitService;
    private final DoctorService doctorService;
    private final ClinicService clinicService;
    private final PatientService patientService;

    public VisitFacade(VisitService visitService, DoctorService doctorService, ClinicService clinicService, PatientService patientService) {
        this.visitService = visitService;
        this.doctorService = doctorService;
        this.clinicService = clinicService;
        this.patientService = patientService;
    }

    public DataPage<VisitDTO> findAll(Pageable page, UUID patientUUID) {
        Page<Visit> visitPage =  visitService.findAll(page, patientUUID);
        return Mapper
                .map(visitPage, page.getPageNumber(), page.getPageSize());
    }

    public DataPage<VisitDTO> findAllWithStatus(Pageable page, UUID patientUUID, VisitStatus status) {
        Page<Visit> visitPage =  visitService.findByPatientUUID(page, patientUUID, status);
        return Mapper
                .map(visitPage, page.getPageNumber(), page.getPageSize());
    }


    public VisitDTO find(UUID uuid) {
        return Mapper.map(visitService.find(uuid));
    }

    public VisitDTO update(UUID uuid, VisitStatus status) {
        return Mapper
                .map(visitService.update(uuid, status));
    }

    public VisitDTO create(NewVisitDTO newVisitDTO) {
        Doctor doctor = doctorService.findByUUID(newVisitDTO.getDoctorUUID());
        Clinic clinic = clinicService.find(newVisitDTO.getClinicUUID());
        Patient patient = patientService.findByUUID(newVisitDTO.getPatientUUID());
        LocalDateTime startDate = newVisitDTO.getStartDate();

        Visit newVisit = visitService.create(doctor, clinic, patient, startDate);

        return Mapper.map(newVisit);
    }
}
