package com.bwas.openclinic.visit.api;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Getter
@Builder
class DoctorDTO {

    private UUID uuid;

    private String firstName;

    private String lastName;
}
