package com.bwas.openclinic.visit.api;

import com.bwas.openclinic.shared.DataPage;
import com.bwas.openclinic.visit.Visit;
import com.bwas.openclinic.visit.services.VisitService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/doctor/visits")
@PreAuthorize("hasRole('ROLE_DOCTOR')")
public class DoctorVisitController {
    private static final String DEFAULT_PAGE_NUMBER = "0";
    private static final String DEFAULT_PAGE_SIZE = "20";
    private static final String DEFAULT_SORT_FIELD = "startDate";
    private static final String DEFAULT_DIRECTION = "ASC";

    private final VisitService visitService;

    public DoctorVisitController(VisitService visitService) {
        this.visitService = visitService;
    }

    @GetMapping
    public DataPage<VisitDTO> findByDoctorUUID(@RequestParam(defaultValue = DEFAULT_PAGE_NUMBER) Integer pageNumber,
                                               @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) Integer pageSize,
                                               @RequestParam(defaultValue = DEFAULT_SORT_FIELD) String sortField,
                                               @RequestParam(defaultValue = DEFAULT_DIRECTION) Sort.Direction direction,
                                               @RequestParam UUID doctorUUID) {
        Pageable page = PageRequest
                .of(pageNumber, pageSize, Sort.by(direction, sortField));
        Page<Visit> visitPage = visitService.findByDoctorUUID(page, doctorUUID);
        return Mapper
                .map(visitPage, pageNumber, pageSize);
    }
}
