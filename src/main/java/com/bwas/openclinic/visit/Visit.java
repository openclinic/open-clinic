package com.bwas.openclinic.visit;

import com.bwas.openclinic.clinic.model.Clinic;
import com.bwas.openclinic.diagnosis.domain.Diagnosis;
import com.bwas.openclinic.shared.AbstractEntity;
import com.bwas.openclinic.user.doctor.domain.Doctor;
import com.bwas.openclinic.user.doctor.domain.Specialization;
import com.bwas.openclinic.user.patient.domain.Patient;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Entity
@Table(name = "visit")
public class Visit extends AbstractEntity {

    @Column(nullable = false)
    private LocalDateTime startDate;

    @Column(nullable = false)
    private LocalDateTime endDate;

    @Setter
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private VisitStatus status = VisitStatus.BOOKED;

    private BigDecimal price;

    private String type;

    @ManyToOne
    @JoinColumn(name = "clinic_id")
    private Clinic clinic;

    @ManyToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @ManyToOne
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    @OneToMany(mappedBy = "visit")
    private Set<Diagnosis> diagnosis = new HashSet<>();

    public Visit() {

    }

    public Visit(LocalDateTime startDate, Specialization specialization, Clinic clinic, Patient patient, Doctor doctor) {
        this.startDate = startDate;
        this.endDate = startDate.plusMinutes(20);
        this.price = specialization.getPrice();
        this.type = specialization.getName();
        this.clinic = clinic;
        this.patient = patient;
        this.doctor = doctor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Visit)) return false;
        Visit visit = (Visit) o;
        return Objects.equals(uuid, visit.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
