package com.bwas.openclinic.visit;

public enum VisitStatus {
    BOOKED("Booked"),
    CANCELED("Canceled"),
    FINISHED("Finished");

    private final String name;

    VisitStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
