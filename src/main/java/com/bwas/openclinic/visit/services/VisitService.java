package com.bwas.openclinic.visit.services;

import com.bwas.openclinic.clinic.model.Clinic;
import com.bwas.openclinic.user.doctor.domain.Doctor;
import com.bwas.openclinic.user.patient.domain.Patient;
import com.bwas.openclinic.visit.Visit;
import com.bwas.openclinic.visit.VisitStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface VisitService {

    Page<Visit> findAll(Pageable page, UUID patientUUID);

    Page<Visit> findByPatientUUID(Pageable page, UUID patientUUID, VisitStatus status);

    Page<Visit> findByDoctorUUID(Pageable page, UUID doctorUUID);

    Visit find(UUID uuid);

    Visit update(UUID uuid, VisitStatus status);

    List<Visit> find(UUID doctorUUID, LocalDateTime startDate, LocalDateTime endDate);

    Visit create(Doctor doctor, Clinic clinic, Patient patient, LocalDateTime startDate);

}
