package com.bwas.openclinic.visit.services;

import com.bwas.openclinic.clinic.model.Clinic;
import com.bwas.openclinic.exceptions.DoctorAlreadyBooked;
import com.bwas.openclinic.exceptions.NotFoundException;
import com.bwas.openclinic.user.doctor.domain.Doctor;
import com.bwas.openclinic.user.patient.domain.Patient;
import com.bwas.openclinic.visit.Visit;
import com.bwas.openclinic.visit.VisitStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
class VisitServiceImpl implements VisitService {

    @Value("${visit.length}")
    private long visitLength;

    private final VisitRepository visitRepository;

    VisitServiceImpl(VisitRepository visitRepository) {
        this.visitRepository = visitRepository;
    }

    @Override
    public Page<Visit> findAll(Pageable page, UUID patientUUID) {
        return visitRepository.findByPatientUuid(patientUUID, page);
    }

    @Override
    public Page<Visit> findByPatientUUID(Pageable page, UUID patientUUID, VisitStatus status) {
        return visitRepository
                .findByPatientUuidAndStatus(patientUUID, status, page);
    }

    @Override
    public Page<Visit> findByDoctorUUID(Pageable page, UUID doctorUUID) {
        return visitRepository
                .findByDoctorUuid(doctorUUID, page);
    }

    @Override
    public Visit find(UUID uuid) {
        return visitRepository.findByUuid(uuid)
                .orElseThrow(() -> new NotFoundException("Visit with UUID" + uuid));
    }

    @Override
    public Visit update(UUID uuid, VisitStatus status) {
        return visitRepository.findByUuid(uuid)
                .map(visit -> {
                    visit.setStatus(status);
                    return visitRepository.save(visit);
                })
                .orElseThrow(() -> new NotFoundException("Visit with UUID" + uuid));
    }

    @Override
    public List<Visit> find(UUID doctorUUID, LocalDateTime startDate, LocalDateTime endDate) {
        return visitRepository.findByDoctorUuidAndEndDateGreaterThanAndStartDateLessThan(doctorUUID, startDate, endDate);
    }

    @Override
    public Visit create(Doctor doctor, Clinic clinic, Patient patient, LocalDateTime startDate) {

        boolean isNotBooked = find(doctor.getUuid(), startDate, startDate.plusMinutes(this.visitLength)).isEmpty();

        if(!isNotBooked) {
            throw new DoctorAlreadyBooked(doctor.getUuid());
        }

        Visit visit = new Visit(startDate, doctor.getSpecialization(), clinic, patient, doctor);
        return visitRepository.save(visit);
    }


}
