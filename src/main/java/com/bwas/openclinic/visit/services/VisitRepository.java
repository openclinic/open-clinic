package com.bwas.openclinic.visit.services;

import com.bwas.openclinic.visit.Visit;
import com.bwas.openclinic.visit.VisitStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
interface VisitRepository extends JpaRepository<Visit, Long> {

    Optional<Visit> findByUuid(UUID uuid);

    Page<Visit> findByPatientUuidAndStatus(UUID patientUUID, VisitStatus status, Pageable pageable);

    Page<Visit> findByDoctorUuid(UUID doctorUUID, Pageable pageable);

    Page<Visit> findByPatientUuid(UUID patientUUID, Pageable pageable);

    List<Visit> findByDoctorUuidAndEndDateGreaterThanAndStartDateLessThan(UUID doctorUUID, LocalDateTime startDate, LocalDateTime endDate);

}
