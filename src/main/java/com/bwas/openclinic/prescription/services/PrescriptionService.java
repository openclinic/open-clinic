package com.bwas.openclinic.prescription.services;

import com.bwas.openclinic.prescription.domain.Prescription;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface PrescriptionService {

    Page<Prescription> findAllByPatientUUID(UUID patientUUID, Pageable page);

    Page<Prescription> findAllByDoctorUUID(UUID doctorUUID, Pageable page);

    Prescription create(Prescription prescription);

}
