package com.bwas.openclinic.prescription.services;

import com.bwas.openclinic.prescription.domain.Prescription;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
class PrescriptionServiceImpl implements PrescriptionService{

    private final PrescriptionRepository prescriptionRepository;

    PrescriptionServiceImpl(PrescriptionRepository prescriptionRepository) {
        this.prescriptionRepository = prescriptionRepository;
    }

    @Override
    public Page<Prescription> findAllByPatientUUID(UUID patientUUID, Pageable page) {
        return prescriptionRepository.findByPatientUuid(patientUUID, page);
    }

    @Override
    public Page<Prescription> findAllByDoctorUUID(UUID doctorUUID, Pageable page) {
        return prescriptionRepository.findByDoctorUuid(doctorUUID,page);
    }

    @Override
    public Prescription create(Prescription prescription) {
        return prescriptionRepository.save(prescription);
    }
}
