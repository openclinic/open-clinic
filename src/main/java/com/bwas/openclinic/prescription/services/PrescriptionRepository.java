package com.bwas.openclinic.prescription.services;

import com.bwas.openclinic.prescription.domain.Prescription;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
interface PrescriptionRepository extends JpaRepository<Prescription, Long> {

    Page<Prescription> findByPatientUuid(UUID patientUUID, Pageable pageable);

    Page<Prescription> findByDoctorUuid(UUID doctorUUID, Pageable pageable);
}
