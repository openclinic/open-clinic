package com.bwas.openclinic.prescription.domain;

import com.bwas.openclinic.shared.AbstractEntity;
import com.bwas.openclinic.user.doctor.domain.Doctor;
import com.bwas.openclinic.user.patient.domain.Patient;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;

@Getter
@Entity
@Table(name = "prescription")
public class Prescription extends AbstractEntity {

    @Column(nullable = false, unique = true)
    private String code;

    @Column(nullable = false)
    private LocalDate dateOfIssuing;

    @Column(nullable = false)
    private LocalDate dateOfExpiration;

    @Column(nullable = false)
    private String medicineName;

    private String description;

    @ManyToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @ManyToOne
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    public Prescription(){}

    public Prescription(String code, LocalDate dateOfIssuing, LocalDate dateOfExpiration, String medicineName, String description, Patient patient, Doctor doctor) {
        this.code = code;
        this.dateOfIssuing = dateOfIssuing;
        this.dateOfExpiration = dateOfExpiration;
        this.medicineName = medicineName;
        this.description = description;
        this.patient = patient;
        this.doctor = doctor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Prescription)) return false;
        Prescription that = (Prescription) o;
        return Objects.equals(uuid, that.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
