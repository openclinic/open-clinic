package com.bwas.openclinic.prescription.api;

import com.bwas.openclinic.prescription.domain.Prescription;
import com.bwas.openclinic.shared.DataPage;
import com.bwas.openclinic.user.doctor.domain.Doctor;
import com.bwas.openclinic.user.patient.domain.Patient;
import org.springframework.data.domain.Page;

import java.util.stream.Collectors;

class Mapper {

    private Mapper(){}

    static DataPage<PrescriptionDTO> map(Page<Prescription> prescriptionPage, Integer pageNumber, Integer pageSize) {
        return DataPage.<PrescriptionDTO>builder()
                .content(prescriptionPage.stream()
                        .map(Mapper::map)
                        .collect(Collectors.toList()))
                .number(pageNumber)
                .size(pageSize)
                .totalElements(prescriptionPage.getTotalElements())
                .build();
    }

    static PrescriptionDTO map(Prescription prescription) {
        Doctor doctor = prescription.getDoctor();
        Patient patient = prescription.getPatient();
        return PrescriptionDTO.builder()
                .uuid(prescription.getUuid())
                .medicineName(prescription.getMedicineName())
                .code(prescription.getCode())
                .dateOfExpiration(prescription.getDateOfExpiration())
                .dateOfIssuing(prescription.getDateOfIssuing())
                .description(prescription.getDescription())
                .doctorName(doctor.getFirstName() + " " + doctor.getLastName())
                .patientName(patient.getFirstName() + " "+ patient.getLastName())
                .build();
    }

}
