package com.bwas.openclinic.prescription.api;

import com.bwas.openclinic.prescription.domain.Prescription;
import com.bwas.openclinic.prescription.services.PrescriptionService;
import com.bwas.openclinic.shared.DataPage;
import com.bwas.openclinic.user.doctor.domain.Doctor;
import com.bwas.openclinic.user.doctor.services.DoctorService;
import com.bwas.openclinic.user.patient.domain.Patient;
import com.bwas.openclinic.user.patient.services.PatientService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.UUID;

@Component
class PrescriptionsFacade {

    private final PrescriptionService prescriptionService;
    private final DoctorService doctorService;
    private final PatientService patientService;

    public PrescriptionsFacade(PrescriptionService prescriptionService, DoctorService doctorService, PatientService patientService) {
        this.prescriptionService = prescriptionService;
        this.doctorService = doctorService;
        this.patientService = patientService;
    }

    DataPage<PrescriptionDTO> findAllByPatientUUID(Pageable page, UUID patientUUID) {
        Page<Prescription> prescriptionPage = prescriptionService.findAllByPatientUUID(patientUUID, page);
        return Mapper
                .map(prescriptionPage, page.getPageNumber(), page.getPageSize());
    }

    DataPage<PrescriptionDTO> findAllByDoctorUUID(Pageable page, UUID doctorUUID) {
        Page<Prescription> prescriptionPage = prescriptionService.findAllByDoctorUUID(doctorUUID, page);
        return Mapper
                .map(prescriptionPage, page.getPageNumber(), page.getPageSize());
    }

    PrescriptionDTO create(NewPrescriptionDTO newPrescriptionDTO) {
        String code = UUID.randomUUID().toString();
        LocalDate dateOfIssuing = LocalDate.now();
        LocalDate dateOfExpiration = dateOfIssuing.plusMonths(4);
        String medicineName = newPrescriptionDTO.getMedicineName();
        String description = newPrescriptionDTO.getDescription();

        Doctor doctor = doctorService.findByUUID(newPrescriptionDTO.getDoctorUUID());
        Patient patient = patientService.findByUUID(newPrescriptionDTO.getPatientUUID());


        Prescription newPrescription = new Prescription(
                code, dateOfIssuing, dateOfExpiration, medicineName, description, patient, doctor
        );

        return Mapper.map(prescriptionService.create(newPrescription));
    }
}
