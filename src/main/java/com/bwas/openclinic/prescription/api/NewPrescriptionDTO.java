package com.bwas.openclinic.prescription.api;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Getter
@Builder
class NewPrescriptionDTO {

    private String medicineName;

    private String description;

    private UUID patientUUID;

    private UUID doctorUUID;

}
