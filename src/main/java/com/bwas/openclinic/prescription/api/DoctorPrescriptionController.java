package com.bwas.openclinic.prescription.api;

import com.bwas.openclinic.shared.DataPage;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/doctor/prescriptions")
@PreAuthorize("hasRole('ROLE_DOCTOR')")
public class DoctorPrescriptionController {
    private static final String DEFAULT_PAGE_NUMBER = "0";
    private static final String DEFAULT_PAGE_SIZE = "20";
    private static final String DEFAULT_SORT_FIELD = "startDate";
    private static final String DEFAULT_DIRECTION = "ASC";

   private final PrescriptionsFacade prescriptionsFacade;

    DoctorPrescriptionController(PrescriptionsFacade prescriptionsFacade) {
        this.prescriptionsFacade = prescriptionsFacade;
    }

    @GetMapping
    public DataPage<PrescriptionDTO> findAll(@RequestParam(defaultValue =  DEFAULT_PAGE_NUMBER) Integer pageNumber,
                                             @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) Integer pageSize,
                                             @RequestParam(defaultValue = DEFAULT_SORT_FIELD) String sortField,
                                             @RequestParam(defaultValue = DEFAULT_DIRECTION) Sort.Direction direction,
                                             @RequestParam UUID doctorUUID) {
        Pageable page = PageRequest
                .of(pageNumber, pageSize, Sort.by(direction, sortField));
       return prescriptionsFacade.findAllByDoctorUUID(page, doctorUUID);
    }

    @PostMapping
    public PrescriptionDTO create(@RequestBody NewPrescriptionDTO newPrescriptionDTO) {
        return prescriptionsFacade.create(newPrescriptionDTO);
    }

}
