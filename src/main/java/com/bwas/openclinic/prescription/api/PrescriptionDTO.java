package com.bwas.openclinic.prescription.api;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Builder
class PrescriptionDTO {

    private UUID uuid;

    private String code;

    private LocalDate dateOfIssuing;

    private LocalDate dateOfExpiration;

    private String medicineName;

    private String description;

    private String doctorName;

    private String patientName;
}
