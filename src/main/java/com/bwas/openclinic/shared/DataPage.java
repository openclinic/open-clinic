package com.bwas.openclinic.shared;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class DataPage<T> {
    private List<T> content;
    private long totalElements;
    private int size;
    private int number;
}
