package com.bwas.openclinic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseDataSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@LiquibaseDataSource
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
