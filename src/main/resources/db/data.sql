INSERT INTO OPEN_CLINIC_DB.user(id, uuid, email, first_name, last_name, phone_number, role, password, active)
VALUES(2, 'bafcd38e-78d1-11ea-bc55-0242ac130003', 'patient@clinic.com', 'Marika', 'Romocka', '888888888', 'PATIENT', '$2a$10$Y1ZHQkZdfcICZr7V5L2LuO6r62CTY8i5KpsX3YfPfCMu5ropqZcK6', true);

INSERT INTO OPEN_CLINIC_DB.user(id, uuid, email, first_name, last_name, phone_number, role, password, active)
VALUES(1, 'fb6539d3-e319-4f39-9ea9-9235e095926e', 'doctor@clinic.com', 'Bartosz', 'Was', '999999999', 'DOCTOR','$2a$10$7wnuPsBKFIzNqNvUssDG0eEGdqIc3f5.871bh0SkWWcvReowbod/6', true);
INSERT INTO OPEN_CLINIC_DB.user(id, uuid, email, first_name, last_name, phone_number, role, password, active)
VALUES(19, 'c01d1ef1-4df6-45df-af7b-2f1212972df8', 'doctor2@clinic.com', 'John', 'Doe', '999999991', 'DOCTOR','$2a$10$7wnuPsBKFIzNqNvUssDG0eEGdqIc3f5.871sh0SkWWcvReowbod/6', true);
INSERT INTO OPEN_CLINIC_DB.user(id, uuid, email, first_name, last_name, phone_number, role, password, active)
VALUES(41, '47d4232c-0f31-493b-93be-0c417c44a1d2', 'doctor3@clinic.com', 'Mark', 'Lensky', '999999998', 'DOCTOR','$2a$10$7wnuPsBKFIzNqNvUssDG0eEGdqIc3f5.871bh6SkWWcvReowbod/6', true);
INSERT INTO OPEN_CLINIC_DB.user(id, uuid, email, first_name, last_name, phone_number, role, password, active)
VALUES(42, '39fa15ac-83f4-11ea-bc55-0242ac130003', 'doctor4@clinic.com', 'John', 'Douglas', '999999992', 'DOCTOR','$2a$10$7wnuPsBKFIzNqNvUssDG0eEGdqIc3f5.871sh0SkWWfvReowbod/6', true);
INSERT INTO OPEN_CLINIC_DB.user(id, uuid, email, first_name, last_name, phone_number, role, password, active)
VALUES(43, '71910634-bbc2-4fb3-86dd-718f26472526', 'doctor5@clinic.com', 'Joshua', 'Mills', '237463527', 'DOCTOR','$2a$10$3wnuPsBKFIzNqNvUssDG0eEGdqIc3f5.871bh0SkWWcvReowbod/6', true);
INSERT INTO OPEN_CLINIC_DB.user(id, uuid, email, first_name, last_name, phone_number, role, password, active)
VALUES(44, 'f10bda8c-855c-11ea-bc55-0242ac130003', 'doctor6@clinic.com', 'Amanda', 'Marks', '947382736', 'DOCTOR','$2a$10$7wnsPsBKFIzNqNvUssDG0eEGdqIc3f5.871sh0SkWWcvReowbod/6', true);
INSERT INTO OPEN_CLINIC_DB.user(id, uuid, email, first_name, last_name, phone_number, role, password, active)
VALUES(45, 'f0f3bb69-9c5d-45f7-a9f5-587468e158a7', 'doctor7@clinic.com', 'Jane', 'Doe', '837463526', 'DOCTOR','$2a$10$7wnuPwBKFIzNqNvUssDG0eEGdqIc3f5.871bh6SkWWcvReowbod/6', true);
INSERT INTO OPEN_CLINIC_DB.user(id, uuid, email, first_name, last_name, phone_number, role, password, active)
VALUES(46, 'f2a791be-0073-4293-aa89-9bf567abef52', 'doctor8@clinic.com', 'Anna', 'Ford', '827364563', 'DOCTOR','$2a$10$73nuPsBKFIzNqNvUssDG0eEGdqIc3f5.871sh0SkWWfvReowbod/6', true);

INSERT INTO OPEN_CLINIC_DB.doctor(id, description, specialization)
VALUES(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ', 'SURGERY');
INSERT INTO OPEN_CLINIC_DB.doctor(id, description, specialization)
VALUES(19, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ', 'CARDIOLOGY');
INSERT INTO OPEN_CLINIC_DB.doctor(id, description, specialization)
VALUES(41, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ', 'SURGERY');
INSERT INTO OPEN_CLINIC_DB.doctor(id, description, specialization)
VALUES(42, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ', 'CARDIOLOGY');
INSERT INTO OPEN_CLINIC_DB.doctor(id, description, specialization)
VALUES(43, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ', 'SURGERY');
INSERT INTO OPEN_CLINIC_DB.doctor(id, description, specialization)
VALUES(44, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ', 'CARDIOLOGY');
INSERT INTO OPEN_CLINIC_DB.doctor(id, description, specialization)
VALUES(45, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ', 'PHYSICAL_MEDICINE');
INSERT INTO OPEN_CLINIC_DB.doctor(id, description, specialization)
VALUES(46, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ', 'GYNECOLOGY');

INSERT INTO OPEN_CLINIC_DB.patient(id, sex, identity_number, date_of_joining, doctor_id)
VALUES(2, 'FEMALE', '1328473627431', '2020-01-03', 1);

INSERT INTO OPEN_CLINIC_DB.clinic(id, uuid, name, street, city, postal_code, phone_number)
VALUES (4,'8702269c-7b42-11ea-bc55-0242ac130003', 'Saints cross clinic', 'new borough', 'London', '05-091', '235948327');
INSERT INTO OPEN_CLINIC_DB.clinic(id, uuid, name, street, city, postal_code, phone_number)
VALUES (5,'133578f9-2cd7-46c4-b048-d2775e6ad169', 'West London clinic', 'Kings cross', 'London', '05-091', '328347382');
INSERT INTO OPEN_CLINIC_DB.clinic(id, uuid, name, street, city, postal_code, phone_number)
VALUES (6,'c2aee8ec-4435-47d4-a39c-35135fe3e4db', 'Westminster clinic', 'Westminster', 'London', '05-091', '347384637');


INSERT INTO OPEN_CLINIC_DB.schedule(id, uuid, start_date, end_date, active, doctor_id)
VALUES(6, '34d209e0-7b43-11ea-bc55-0242ac130003', '2020-01-03', null, true, 1);
INSERT INTO OPEN_CLINIC_DB.schedule(id, uuid, start_date, end_date, active, doctor_id)
VALUES(7, '38bc6993-4a96-49ff-8f72-738511de4854', '2020-01-03', null, true, 19);
INSERT INTO OPEN_CLINIC_DB.schedule(id, uuid, start_date, end_date, active, doctor_id)
VALUES(8, '373fa762-583e-4d22-bbe7-71695c1ddff6', '2020-01-03', null, true, 41);
INSERT INTO OPEN_CLINIC_DB.schedule(id, uuid, start_date, end_date, active, doctor_id)
VALUES(10, '088dc8c7-6b41-4f49-bcd8-01819ce36e89', '2020-01-03', null, true, 43);
INSERT INTO OPEN_CLINIC_DB.schedule(id, uuid, start_date, end_date, active, doctor_id)
VALUES(11, '13cb8b04-6cd1-4d76-a9f9-2e8fd2d1fb23', '2020-01-03', null, true, 44);
INSERT INTO OPEN_CLINIC_DB.schedule(id, uuid, start_date, end_date, active, doctor_id)
VALUES(12, '3fcea998-10e4-419f-8696-980f8067c7a3', '2020-01-03', null, true, 46);
INSERT INTO OPEN_CLINIC_DB.schedule(id, uuid, start_date, end_date, active, doctor_id)
VALUES(13, 'e63f7148-ec87-4f25-b863-d39316be0e7c', '2020-02-04', null, true, 45);
INSERT INTO OPEN_CLINIC_DB.schedule(id, uuid, start_date, end_date, active, doctor_id)
VALUES(14, '93d6703a-0e80-4d60-9703-3193d069abc7', '2020-02-04', null, true, 42);
INSERT INTO OPEN_CLINIC_DB.schedule(id, uuid, start_date, end_date, active, doctor_id)
VALUES(15, 'd6cbdcd5-f6e6-425e-8d23-f0a237628f79', '2019-09-04', '2020-01-03', false, 43);



INSERT INTO OPEN_CLINIC_DB.schedule_entry(id, uuid, day, start_hour, end_hour, clinic_id, schedule_id)
VALUES(7, '71471352-7b43-11ea-bc55-0242ac130003', 'MONDAY', '08:00:00', '17:00:00', 4, 6);
INSERT INTO OPEN_CLINIC_DB.schedule_entry(id, uuid, day, start_hour, end_hour, clinic_id, schedule_id)
VALUES(8, '1582b048-7b44-11ea-bc55-0242ac130003', 'TUESDAY', '08:00:00', '17:00:00', 4, 6);
INSERT INTO OPEN_CLINIC_DB.schedule_entry(id, uuid, day, start_hour, end_hour, clinic_id, schedule_id)
VALUES(9, '29fc8a62-7b44-11ea-bc55-0242ac130003', 'WEDNESDAY', '08:00:00', '17:00:00', 4, 6);
INSERT INTO OPEN_CLINIC_DB.schedule_entry(id, uuid, day, start_hour, end_hour, clinic_id, schedule_id)
VALUES(10, '65b6efe8-7b44-11ea-bc55-0242ac130003', 'THURSDAY', '08:00:00', '17:00:00', 4, 6);
INSERT INTO OPEN_CLINIC_DB.schedule_entry(id, uuid, day, start_hour, end_hour, clinic_id, schedule_id)
VALUES(11, '7f387e96-7b44-11ea-bc55-0242ac130003', 'FRIDAY', '08:00:00', '17:00:00', 4, 6);

INSERT INTO OPEN_CLINIC_DB.schedule_entry(id, uuid, day, start_hour, end_hour, clinic_id, schedule_id)
VALUES(12, '71471352-7b43-11ea-bc55-0242ac130004', 'MONDAY', '08:00:00', '17:00:00', 4, 8);
INSERT INTO OPEN_CLINIC_DB.schedule_entry(id, uuid, day, start_hour, end_hour, clinic_id, schedule_id)
VALUES(13, '1582b048-7b44-11ea-bc55-0242ac130005', 'TUESDAY', '08:00:00', '17:00:00', 4, 8);
INSERT INTO OPEN_CLINIC_DB.schedule_entry(id, uuid, day, start_hour, end_hour, clinic_id, schedule_id)
VALUES(14, '29fc8a62-7b44-11ea-bc55-0242ac130006', 'WEDNESDAY', '08:00:00', '17:00:00', 4, 8);
INSERT INTO OPEN_CLINIC_DB.schedule_entry(id, uuid, day, start_hour, end_hour, clinic_id, schedule_id)
VALUES(15, '65b6efe8-7b44-11ea-bc55-0242ac130007', 'THURSDAY', '08:00:00', '17:00:00', 4, 8);
INSERT INTO OPEN_CLINIC_DB.schedule_entry(id, uuid, day, start_hour, end_hour, clinic_id, schedule_id)
VALUES(16, '7f387e96-7b44-11ea-bc55-0242ac130008', 'FRIDAY', '08:00:00', '17:00:00', 4, 8);
